Day 8: Seven Segment Search
===========================

https://adventofcode.com/2021/day/8

I started off doing this one with the idea "hey it'd be really cool if I didn't hardcode a solution and instead let the program figure out the associations itself with a lot of trials and eliminations," started coding that for a while, and then said "actually this sucks I'll hardcore a solution"

So the hardcoded solution is to identify what each segment really is, and then in the output you can just replace each segment in a digit with what it really is and then read what that digit is from a table. The process to identify each segment is based on the following observation of the total number of times each segment is used in the digits 0-9:

* `a`: 8
* `b`: 6
* `c`: 8
* `d`: 7
* `e`: 4
* `f`: 9
* `g`: 7

So we can identify `b`, `e`, and `f` immediately by counting the total numbers of each segment in the scrambled signal patterns. For the others, we'll need to identify which patterns encode the digits 1, 7, and 4, which happily part 1 literally tells you how to do. With those, segment `a` is the segment found in the digit 7 and not in the digit 1, and so segment `c` is the other segment of count 8. Also, segment `d` is the segment of count 7 that's found in the digit 4, while segment `g` is the one that's not.
