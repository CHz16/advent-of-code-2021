//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "8", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var digits = 0
input.enumerateLines { line, stop in
    let components = line.components(separatedBy: " ")
    for component in components.dropFirst(11) {
        if component.count == 2 || component.count == 3 || component.count == 4 || component.count == 7 {
            digits += 1
        }
    }
}
print(digits)


// Part 2

print("--------------")

let digitDefs = [
    "abcefg": 0,
    "cf": 1,
    "acdeg": 2,
    "acdfg": 3,
    "bcdf": 4,
    "abdfg": 5,
    "abdefg": 6,
    "acf": 7,
    "abcdefg": 8,
    "abcdfg": 9
]

var sum = 0
input.enumerateLines { line, stop in
    let components = line.components(separatedBy: " ")

    let defs = components.dropLast(5).map { String($0.sorted()) }.sorted { $0.count < $1.count }
    let segmentCounts: [Character: Int] = defs.reduce(into: [:]) { count, segments in
        for segment in segments {
            count[segment, default: 0] += 1
        }
    }

    let aSegment = Set(defs[1]).subtracting(Set(defs[0])).first!
    var segments: [Character: Character] = [:]
    segments[aSegment] = "a"
    for (segment, count) in segmentCounts {
        if count == 6 {
            segments[segment] = "b"
        } else if count == 4 {
            segments[segment] = "e"
        } else if count == 9 {
            segments[segment] = "f"
        } else if count == 8 && segment != aSegment {
            segments[segment] = "c"
        } else if count == 7 {
            if defs[2].contains(segment) {
                segments[segment] = "d"
            } else {
                segments[segment] = "g"
            }
        }
    }

    let digits = components.dropFirst(11).map { digitDefs[String($0.map { segments[$0]! }.sorted())]! }
    let output = 1000 * digits[0] + 100 * digits[1] + 10 * digits[2] + digits[3]
    print(output)
    sum += output
}
print(sum)
