#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "22.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

enum Axis { case x, y, z }

struct Cube: CustomStringConvertible {
    let xMin, xMax, yMin, yMax, zMin, zMax: Int

    func intersects(_ cube2: Cube) -> Bool {
        return !(cube2.xMin > xMax) && !(cube2.xMax < xMin) && !(cube2.yMin > yMax) && !(cube2.yMax < yMin) && !(cube2.zMin > zMax) && !(cube2.zMax < zMin)
    }

    var volume: Int {
        (xMax - xMin + 1) * (yMax - yMin + 1) * (zMax - zMin + 1)
    }

    var description: String {
        "(\(xMin)..\(xMax),\(yMin)..\(yMax),\(zMin)..\(zMax))"
    }

    func split(at coord: Int, along axis: Axis) -> [Cube] {
        switch axis {
        case .x:
            return [Cube(xMin: xMin, xMax: coord - 1, yMin: yMin, yMax: yMax, zMin: zMin, zMax: zMax), Cube(xMin: coord, xMax: xMax, yMin: yMin, yMax: yMax, zMin: zMin, zMax: zMax)]
        case .y:
            return [Cube(xMin: xMin, xMax: xMax, yMin: yMin, yMax: coord - 1, zMin: zMin, zMax: zMax), Cube(xMin: xMin, xMax: xMax, yMin: coord, yMax: yMax, zMin: zMin, zMax: zMax)]
        case .z:
            return [Cube(xMin: xMin, xMax: xMax, yMin: yMin, yMax: yMax, zMin: zMin, zMax: coord - 1), Cube(xMin: xMin, xMax: xMax, yMin: yMin, yMax: yMax, zMin: coord, zMax: zMax)]
        }
    }

    func segment(source: Cube) -> [Cube] {
        if !intersects(source) {
            return [self]
        }

        var target = self
        var targetSegments: [Cube] = []
        if xMin < source.xMin {
            let splits = target.split(at: source.xMin, along: .x)
            targetSegments.append(splits[0])
            target = splits[1]
        }
        if xMax > source.xMax {
            let splits = target.split(at: source.xMax + 1, along: .x)
            targetSegments.append(splits[1])
            target = splits[0]
        }
        if yMin < source.yMin {
            let splits = target.split(at: source.yMin, along: .y)
            targetSegments.append(splits[0])
            target = splits[1]
        }
        if yMax > source.yMax {
            let splits = target.split(at: source.yMax + 1, along: .y)
            targetSegments.append(splits[1])
            target = splits[0]
        }
        if zMin < source.zMin {
            let splits = target.split(at: source.zMin, along: .z)
            targetSegments.append(splits[0])
            target = splits[1]
        }
        if zMax > source.zMax {
            let splits = target.split(at: source.zMax + 1, along: .z)
            targetSegments.append(splits[1])
            target = splits[0]
        }
        return targetSegments
    }

}

var cubes: [Cube] = []
input.enumerateLines { line, stop in
    let components = line.components(separatedBy: " ")
    let coords = components.dropFirst().compactMap { Int($0) }
    if !(coords.filter { $0 > 50 || $0 < -50 }.isEmpty) {
        return
    }

    let cube = Cube(xMin: coords[0], xMax: coords[1], yMin: coords[2], yMax: coords[3], zMin: coords[4], zMax: coords[5])
    cubes = cubes.flatMap { $0.segment(source: cube) }
    if components[0] == "on" {
        cubes.append(cube)
    }
}
print(cubes.map { $0.volume }.reduce(0, +))


// Part 2

print("--------------")

input.enumerateLines { line, stop in
    let components = line.components(separatedBy: " ")
    let coords = components.dropFirst().compactMap { Int($0) }
    if (coords.filter { $0 > 50 || $0 < -50 }.isEmpty) {
        return
    }

    let cube = Cube(xMin: coords[0], xMax: coords[1], yMin: coords[2], yMax: coords[3], zMin: coords[4], zMax: coords[5])
    cubes = cubes.flatMap { $0.segment(source: cube) }
    if components[0] == "on" {
        cubes.append(cube)
    }
}
print(cubes.map { $0.volume }.reduce(0, +))
