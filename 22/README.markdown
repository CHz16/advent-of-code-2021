Day 22: Reactor Reboot
======================

https://adventofcode.com/2021/day/22

I had literally no idea how to do the math for this puzzle, but I was really determined to figure it out, so I came up with a vague approach and then flailed at it for ages, progressively refining the idea as I thought of things that could be done better. I eventually landed on something that's pretty clean, so that felt good.

The base block was to have a function that splits a rectangular prism into two prisms along a given axes at a coordinate. That's used in a function on a prism that, given a second prism, segments the volume NOT overlapped by the second prism into a list of smaller prisms and returns that. This is actually pretty easy with the observation that if the prism we're splitting intersects with a given face of the second prism, we can split the the first prism along that face into a prism that lies completely outside the second prism and one that still intersects the second prism somewhere. We can then keep splitting that remainder along the other five faces as necessary, keeping the parts that lie outside the second prism, and then just throw away the remainder at the end because what's left lies completely inside the second prism.

With this segmentation function, we can implement adding and subtracting prisms in such a way that we get a list of prisms that cover exactly the area of that operation. To add prisms A and B, we segment A using B, and then the combined area are the segments of A along with B. To subtract B from A, we do the same thing but just keep the segments of A and throw B away after. So to actually do the puzzle, we keep a list of active prisms and go through every prism in the input, segmenting every prism in the active list by it and then adding that new prism to the list at the end if it's an on line. The volume calculation at the end is trivial.

* Part 1: 6029th place (2:52:54)
* Part 2: 1809th place (2:55:23)
