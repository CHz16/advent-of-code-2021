//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "3", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var numberLength = 0
for char in input {
    if char == "\n" {
        break
    }
    numberLength += 1
}

var zeroes = Array(repeating: 0, count: numberLength)
var ones = Array(repeating: 0, count: numberLength)
var numberOfNumbers = 0
input.enumerateLines { line, stop in
    numberOfNumbers += 1
    for (i, char) in line.enumerated() {
        if char == "0" {
            zeroes[i] += 1
        } else {
            ones[i] += 1
        }
    }
}

var gamma = 0, epsilon = 0
for i in 0..<numberLength {
    gamma *= 2
    epsilon *= 2
    if zeroes[i] > ones[i] {
        epsilon += 1
    } else {
        gamma += 1
    }
}
print("\(gamma) * \(epsilon) = \(gamma * epsilon)")


// Part 2

print("--------------")

var candidates = input.components(separatedBy: "\n").dropLast()
for i in 0..<numberLength {
    var zeroes = 0, ones = 0
    for candidate in candidates {
        if candidate[candidate.index(candidate.startIndex, offsetBy: i)] == "0" {
            zeroes += 1
        } else {
            ones += 1
        }
    }
    if zeroes > ones {
        candidates = candidates.filter { $0[$0.index($0.startIndex, offsetBy: i)] == "0" }
    } else {
        candidates = candidates.filter { $0[$0.index($0.startIndex, offsetBy: i)] == "1" }
    }
    if candidates.count == 1 {
        break
    }
}
var oxygen = 0
for char in candidates[0] {
    oxygen *= 2
    if char == "1" {
        oxygen += 1
    }
}

candidates = input.components(separatedBy: "\n").dropLast()
for i in 0..<numberLength {
    var zeroes = 0, ones = 0
    for candidate in candidates {
        if candidate[candidate.index(candidate.startIndex, offsetBy: i)] == "0" {
            zeroes += 1
        } else {
            ones += 1
        }
    }
    if ones > zeroes || zeroes == ones {
        candidates = candidates.filter { $0[$0.index($0.startIndex, offsetBy: i)] == "0" }
    } else {
        candidates = candidates.filter { $0[$0.index($0.startIndex, offsetBy: i)] == "1" }
    }
    if candidates.count == 1 {
        break
    }
}
var co2 = 0
for char in candidates[0] {
    co2 *= 2
    if char == "1" {
        co2 += 1
    }
}

print("\(oxygen) * \(co2) = \(oxygen * co2)")
