Day 3: Binary Diagnostic
========================

https://adventofcode.com/2021/day/3

Messed up a couple of times while solving this one: first for some reason I wrote the binary number conversion in the wrong direction and reversed the bits, and second I didn't read the problem description completely (classic) and missed the tiebreaker condition. But I still hopped up 900 places on part 2, so that's cool I guess?

I made literally no attempt whatsoever at good code, just copy-pasted previous steps and tweaked them slightly until they worked. So it's super gross in there, watch out for that

* Part 1: 1574th place (7:23)
* Part 2: 663rd place (17:49)
