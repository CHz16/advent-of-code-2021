Day 25: Sea Cucumber
====================

https://adventofcode.com/2021/day/25

We end, as tradition, with a single part easy puzzle (part 2 just being a check if you've solved all previous 49 parts). I beefed this one by assuming the generation algorithm I immediately came up with would work, and spoiler: it didn't! It cost me a few minutes, but I don't think it cost five whole minutes, so I probably wouldn't have placed anyway.

* Part 1: 440th place (14:41)
* Part 2: 367th place (14:44)
