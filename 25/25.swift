#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "25.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

enum Cell { case empty, east, south }

var startingGrid: [[Cell]] = []
input.enumerateLines { line, stop in
    startingGrid.append(line.map {
        if $0 == ">" {
            return .east
        } else if $0 == "v" {
            return .south
        } else {
            return .empty
        }
    })
}
let width = startingGrid[0].count, height = startingGrid.count

var grid = startingGrid
var turn = 1
while true {
    var moved = false
    var move: [(Int, Int)] = []
    for row in 0..<height {
        for col in 0..<width {
            if grid[row][col] == .east && grid[row][(col + 1) % width] == .empty {
                move.append((row, col))
                moved = true
            }
        }
    }
    for (row, col) in move {
        grid[row][col] = .empty
        grid[row][(col + 1) % width] = .east
    }
    move = []
    for row in 0..<height {
        for col in 0..<width {
            if grid[row][col] == .south && grid[(row + 1) % height][col] == .empty {
                move.append((row, col))
                moved = true
            }
        }
    }
    for (row, col) in move {
        grid[row][col] = .empty
        grid[(row + 1) % height][col] = .south
    }
    if !moved {
        break
    }
    turn += 1
}
print(turn)
