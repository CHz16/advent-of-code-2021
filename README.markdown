Advent of Code 2021
===================

These are my solutions in Swift to [Advent of Code 2021](https://adventofcode.com/2021), a kind of fun series of 25 programming puzzles that ran in December 2021. I figured I'd throw these up on the web somewhere, because why not. If I was competing for a leaderboard spot that day (that is, I started coding at 9 PM Pacific), then the readme file for that day will give my time and place.

While the challenges had no time limit, there was a leaderboard that showed (essentially) the fastest 100 solvers of each day's problem, so naturally I tried to solve them as quickly as possible. So, most of the solutions here are simply what left my fingers the fastest. That's speedcoding for you.

Every user had their puzzle inputs randomly selected from a pool of possibilities, so the inputs you see in these files may not be the same ones you'd get were you to sign up. I also tend to preprocess them in a text editor to make them way easier to parse.

Partway through this year I decided to make a serious effort to get everything working in a playground, and then day 15 walked up and socked me right in the gob, stating emphatically that that would never happen. Oh well, maybe next year (but probably not)

* Best placement for any part: day 16, part 1 (53rd place)
* Days with later optimizations or bugfixes: 6, 7, 12, 15, 19, 24
* Favorite solution: day 22

*See also: [2015](https://bitbucket.org/CHz16/advent-of-code-2015), [2016](https://bitbucket.org/CHz16/advent-of-code-2016), [2017](https://bitbucket.org/CHz16/advent-of-code-2017), [2018](https://bitbucket.org/CHz16/advent-of-code-2018), [2019](https://bitbucket.org/CHz16/advent-of-code-2019), [2020](https://bitbucket.org/CHz16/advent-of-code-2020), [2022](https://bitbucket.org/CHz16/advent-of-code-2022)*
