Day 21: Dirac Dice
==================

https://adventofcode.com/2021/day/21

Did pretty well on part 1, and then totally beefed part 2 because I inexplicably decided to not implement the game as described and instead made up a completely different game and solved that. Solved it three different times, because I got the wrong answer for the puzzle every time (on account if it being the wrong game)! Once I actually reread the rules and got to the part where it says "The game is played the same as before," it didn't take a whole lot more time to code that. After which I kept getting the wrong answers again, because I used the final positions from part 1 as the starting positions in part 2 instead of the `startingPositions` variable. So not a great showing on my part today

Part 1 is just a direct simulation of the game. In part 2 we do a search across every possible state of the game, where a state is the current scores and positions of the players and whose turn is next, along with how many possible ways we've encountered to get to that state. When we pop a state, if it's a win state then we add the number of paths to it to the correct player's total, and if it's still a live state then we find all states that can be reached from there in one turn, add (the number of paths to the current state * the number of rolls that reach that future state) to the number of paths to that future state, and add that future state to the queue.

The queue needs to return states in such an order that we've visited all possible predecessors of a state before we pop it, which might seem tricky considering the positional and turn order data, but luckily there's a fairly easy way to ensure this: by the rules of the game, any move will result in a player gaining at least one point, so if we consider the players' scores as a 2D coordinate with (0, 0) in the bottom left, all of a state's predecessor states will either be directly left or below of that state. Thus, if we walk along rows from left to right, starting at the bottom and working our way up, we'll have an order that works fine. The players' positions on the board and whose turn it is don't matter for this ordering at all!

* Part 1: 384th place (8:11)
* Part 2: 2252nd place (1:36:16)
