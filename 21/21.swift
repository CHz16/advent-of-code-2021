#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "21.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Implementation from Introduction to Algorithms, Second Edition (CLRS)
struct PriorityQueue<ID: Hashable, Weight: Comparable> {

    typealias Entry = (id: ID, weight: Weight)

    var heap: [Entry] = []
    var count: Int
    var lookup: [ID: Int] = [:]

    init() {
        self.init(items: [])
    }

    init(items: [Entry]) {
        heap = items
        count = heap.count
        for (i, (id: id, weight: _)) in heap.enumerated() {
            lookup[id] = i
        }

        for i in (0..<(count / 2)).reversed() {
            minHeapify(root: i)
        }
    }

    var isEmpty: Bool {
        return count == 0
    }

    var min: Entry {
        return heap[0]
    }

    mutating func removeMin() -> Entry {
        return remove(i: 0)
    }

    mutating func remove(id: ID) -> Entry {
        return remove(i: lookup[id]!)
    }

    mutating func reduceWeight(of id: ID, to newWeight: Weight) {
        var i = lookup[id]!
        // This check could be <=, because if an entry is in the right place, then there's no need to do anything if we're setting the weight to the same value. However, the implementation of insertItem uses this method to force a heap rebalancing for the entry it inserts into potentially the *wrong* place, and so in that case it's useful to let it run.
        if heap[i].weight < newWeight {
            return
        }

        heap[i] = Entry(id: heap[i].id, weight: newWeight)
        while i > 0 && heap[parent(i)].weight > heap[i].weight {
            exchange(i, parent(i))
            i = parent(i)
        }
    }

    mutating func insertItem(id: ID, weight: Weight) {
        count += 1
        let newEntry = Entry(id: id, weight: weight)
        if heap.count < count {
            heap.append(newEntry)
        } else {
            heap[count - 1] = newEntry
        }
        lookup[newEntry.id] = count - 1
        reduceWeight(of: id, to: weight)
    }


    mutating func minHeapify(root i: Int) {
        let leftIndex = left(i)
        let rightIndex = right(i)

        var smallest: Int
        if leftIndex < count && heap[leftIndex].weight < heap[i].weight {
            smallest = leftIndex
        } else {
            smallest = i
        }
        if rightIndex < count && heap[rightIndex].weight < heap[smallest].weight {
            smallest = rightIndex
        }

        if smallest != i {
            exchange(smallest, i)
            minHeapify(root: smallest)
        }
    }

    mutating func remove(i: Int) -> Entry {
        let entry = heap[i]

        lookup[entry.id] = nil
        count -= 1
        if i < count {
            lookup[heap[count].id] = i
            heap[i] = heap[count]
            minHeapify(root: i)
        }

        return entry
    }

    func parent(_ i: Int) -> Int {
        return i / 2
    }

    func left(_ i: Int) -> Int {
        return 2 * i
    }

    func right(_ i: Int) -> Int {
        return 2 * i + 1
    }

    mutating func exchange(_ i1: Int, _ i2: Int) {
        lookup[heap[i1].id] = i2
        lookup[heap[i2].id] = i1
        (heap[i1], heap[i2]) = (heap[i2], heap[i1])
    }
}


// Part 1

let startingPositions = input.components(separatedBy: .whitespacesAndNewlines).compactMap { Int($0) }.map { $0 - 1 }

var die = 1, rolls = 0
var positions = startingPositions
var scores = Array(repeating: 0, count: startingPositions.count)
game: while true {
    for i in 0..<positions.count {
        positions[i] = (positions[i] + 3 * die + 3) % 10
        scores[i] += positions[i] + 1
        die += 3
        rolls += 3
        if scores[i] >= 1000 {
            break game
        }
    }
}
print("\(scores.min()!) * \(rolls) = \(scores.min()! * rolls)")


// Part 2

print("--------------")

struct State: Hashable {
    let p1Position, p2Position: Int
    let p1Score, p2Score: Int
    let p1Next: Bool
}

let nexts = [
    (increase: 3, options: 1),
    (increase: 4, options: 3),
    (increase: 5, options: 6),
    (increase: 6, options: 7),
    (increase: 7, options: 6),
    (increase: 8, options: 3),
    (increase: 9, options: 1)
]

let initialState = State(p1Position: startingPositions[0], p2Position: startingPositions[1], p1Score: 0, p2Score: 0, p1Next: true)
var queue = PriorityQueue(items: [(id: initialState, weight: 0)])
var seen = Set([initialState])
var ways = [initialState: 1]
var p1Wins = 0, p2Wins = 0
while !queue.isEmpty {
    let state = queue.removeMin().id
    print(state, queue.count)
    if state.p1Score >= 21 {
        p1Wins += ways[state]!
        continue
    } else if state.p2Score >= 21 {
        p2Wins += ways[state]!
        continue
    }

    for (increase, options) in nexts {
        let next: State
        if state.p1Next {
            let newPosition = (state.p1Position + increase) % 10
            next = State(p1Position: newPosition, p2Position: state.p2Position, p1Score: state.p1Score + newPosition + 1, p2Score: state.p2Score, p1Next: false)
        } else {
            let newPosition = (state.p2Position + increase) % 10
            next = State(p1Position: state.p1Position, p2Position: newPosition, p1Score: state.p1Score, p2Score: state.p2Score + newPosition + 1, p1Next: true)
        }
        ways[next, default: 0] += options * ways[state]!
        if !seen.contains(next) {
            seen.insert(next)
            queue.insertItem(id: next, weight: next.p1Score * 50 + next.p2Score)
        }
    }
}
print(p1Wins)
print(p2Wins)
