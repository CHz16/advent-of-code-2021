//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "6", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

let starterFish = input.components(separatedBy: "\n")[0].components(separatedBy: ",").compactMap { Int($0) }

var currentFish = [0, 0, 0, 0, 0, 0, 0, 0, 0]
for fish in starterFish {
    currentFish[fish] += 1
}

for _ in 0..<80 {
    let zeroes = currentFish[0]
    for i in 0..<8 {
        currentFish[i] = currentFish[i + 1]
    }
    currentFish[6] += zeroes
    currentFish[8] = zeroes
}
print(currentFish)
print(currentFish.reduce(0, +))


// Part 2

print("--------------")

for _ in 0..<176 {
    let zeroes = currentFish[0]
    for i in 0..<8 {
        currentFish[i] = currentFish[i + 1]
    }
    currentFish[6] += zeroes
    currentFish[8] = zeroes
}
print(currentFish)
print(currentFish.reduce(0, +))
