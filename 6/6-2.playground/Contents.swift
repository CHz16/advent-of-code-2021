//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "6", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

let starterFish = input.components(separatedBy: "\n")[0].components(separatedBy: ",").compactMap { Int($0) }

var currentFish = [0, 0, 0, 0, 0, 0, 0, 0, 0]
for fish in starterFish {
    currentFish[fish] += 1
}

var i = 0
while i < 80 {
    currentFish[(i + 7) % 9] += currentFish[i % 9]
    i += 1
}
print(currentFish)
print(currentFish.reduce(0, +))


// Part 2

print("--------------")

while i < 256 {
    currentFish[(i + 7) % 9] += currentFish[i % 9]
    i += 1
}
print(currentFish)
print(currentFish.reduce(0, +))
