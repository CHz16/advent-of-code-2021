Day 6: Lanternfish
==================

https://adventofcode.com/2021/day/6

Ah, we've reached the first "find a better way to do this or else it'll take one trillion years to execute" puzzle of the year. Since all the fish are fungible, you just have to keep track of how many you have of every possible timer value. Then, the time step calculation is to rotate the entire array one to the left (fish with timers 1-8 become 0-7, and fish with timers 0 become 8 as the new generation) and also add the number of fish that rotated from 0 to 8 (the breeding generation) to the timer 6 population.

UPDATE: While writing this text file, I only realized after typing the word "rotate" that I was indeed rotating the array, and as such didn't actually have to waste computer instructions doing that when I could instead just keep track of the index of whatever element's at the front. This simplifies the time step calculation to just adding `fish[i]` to `fish[i + 7]` and then adding 1 to `i`, with proper modulo wrapping on `i`.
