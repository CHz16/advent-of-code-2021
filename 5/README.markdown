Day 5: Hydrothermal Venture
===========================

https://adventofcode.com/2021/day/5

Felt pretty good about how quickly I banged this one out, but still 79 whole places out of the leaderboard oops. Another copy-paste job, part 2 just reuses the results from part 1 and then adds the diagonal line points on top.

* Part 1: 389th place (8:06)
* Part 2: 179th place (10:45)
