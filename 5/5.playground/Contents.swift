//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "5", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var points: [Int: [Int: Int]] = [:]
input.enumerateLines { line, stop in
    let coords = line.components(separatedBy: " ").compactMap { Int($0) }
    if coords[0] != coords[2] && coords[1] != coords[3] {
        return
    }

    let delta: (x: Int, y: Int)
    if coords[0] != coords[2] {
        delta = (x: coords[0] > coords[2] ? -1 : 1, y: 0)
    } else {
        delta = (x: 0, y: coords[1] > coords[3] ? -1 : 1)
    }

    var x = coords[0], y = coords[1]
    while x != coords[2] || y != coords[3] {
        points[x, default: [:]][y, default: 0] += 1
        x += delta.x
        y += delta.y
    }
    points[x, default: [:]][y, default: 0] += 1
}

var overlaps = 0
for (_, ys) in points {
    for (_, score) in ys {
        if score > 1 {
            overlaps += 1
        }
    }
}
print(overlaps)


// Part 2

print("--------------")

input.enumerateLines { line, stop in
    let coords = line.components(separatedBy: " ").compactMap { Int($0) }
    if coords[0] == coords[2] || coords[1] == coords[3] {
        return
    }

    let xDelta = coords[0] > coords[2] ? -1 : 1
    let yDelta = coords[1] > coords[3] ? -1 : 1

    var x = coords[0], y = coords[1]
    while x != coords[2] || y != coords[3] {
        points[x, default: [:]][y, default: 0] += 1
        x += xDelta
        y += yDelta
    }
    points[x, default: [:]][y, default: 0] += 1
}

overlaps = 0
for (_, ys) in points {
    for (_, score) in ys {
        if score > 1 {
            overlaps += 1
        }
    }
}
print(overlaps)
