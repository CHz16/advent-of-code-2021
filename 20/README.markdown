Day 20: Trench Map
==================

https://adventofcode.com/2021/day/20

Starts off looking like your bog standard Advent of Code infinite cellular automaton puzzle, so I did my usual business of keeping track of all the cells that are on in a set instead of preallocating a bunch of space in a 2D array or constantly expanding it. Got the right answer on the test data, plugged in my puzzle input, and then promptly got the wrong answer repeatedly.

Because there's a trick in the actual puzzle data, where index 0 is # and index 511 is ., meaning the empty space around the image will toggle on and off every generation. So the set approach doesn't work, because cells past the known frontier will all be on on odd generations. I just hardcoded this into my solution and it worked fine after that.

* Part 1: 598th place (29:24)
* Part 2: 443th place (29:53)
