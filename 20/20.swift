#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "20.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

struct Point: Hashable { let row, col: Int }

let lines = input.components(separatedBy: "\n")
let lookup = lines[0].map { $0 == "#" }

var startingImage: Set<Point> = Set()
for (row, line) in lines.dropFirst(2).enumerated() {
    for (col, char) in line.enumerated() {
        if char == "#" {
            startingImage.insert(Point(row: row, col: col))
        }
    }
}

var image = startingImage
var minRow = 0, maxRow = lines.count - 4
var minCol = 0, maxCol = lines[2].count - 1
var outOfBounds = false
for i in 0..<2 {
    if i % 2 == 0 {
        print(image.count, "[\(minRow),\(maxRow)]", "[\(minCol),\(maxCol)]")
    }
    var newImage: Set<Point> = Set()
    var newMinRow = Int.max, newMaxRow = Int.min
    var newMinCol = Int.max, newMaxCol = Int.min
    for row in (minRow - 1)...(maxRow + 1) {
        for col in (minCol - 1)...(maxCol + 1) {
            var index = 0
            for probeRow in (row - 1)...(row + 1) {
                for probeCol in (col - 1)...(col + 1) {
                    index *= 2
                    if image.contains(Point(row: probeRow, col: probeCol)) {
                        index += 1
                    } else if outOfBounds && (probeRow < minRow || probeRow > maxRow || probeCol < minCol || probeCol > maxCol) {
                        index += 1
                    }
                }
            }
            if lookup[index] {
                newMinRow = min(newMinRow, row)
                newMaxRow = max(newMaxRow, row)
                newMinCol = min(newMinCol, col)
                newMaxCol = max(newMaxCol, col)
                newImage.insert(Point(row: row, col: col))
            }
        }
    }
    minRow = newMinRow
    maxRow = newMaxRow
    minCol = newMinCol
    maxCol = newMaxCol
    image = newImage
    outOfBounds = !outOfBounds
}
print(image.count, "[\(minRow),\(maxRow)]", "[\(minCol),\(maxCol)]")


// Part 2

print("--------------")

for i in 2..<50 {
    if i % 2 == 0 {
        print(image.count, "[\(minRow),\(maxRow)]", "[\(minCol),\(maxCol)]")
    }
    var newImage: Set<Point> = Set()
    var newMinRow = Int.max, newMaxRow = Int.min
    var newMinCol = Int.max, newMaxCol = Int.min
    for row in (minRow - 1)...(maxRow + 1) {
        for col in (minCol - 1)...(maxCol + 1) {
            var index = 0
            for probeRow in (row - 1)...(row + 1) {
                for probeCol in (col - 1)...(col + 1) {
                    index *= 2
                    if image.contains(Point(row: probeRow, col: probeCol)) {
                        index += 1
                    } else if outOfBounds && (probeRow < minRow || probeRow > maxRow || probeCol < minCol || probeCol > maxCol) {
                        index += 1
                    }
                }
            }
            if lookup[index] {
                newMinRow = min(newMinRow, row)
                newMaxRow = max(newMaxRow, row)
                newMinCol = min(newMinCol, col)
                newMaxCol = max(newMaxCol, col)
                newImage.insert(Point(row: row, col: col))
            }
        }
    }
    minRow = newMinRow
    maxRow = newMaxRow
    minCol = newMinCol
    maxCol = newMaxCol
    image = newImage
    outOfBounds = !outOfBounds
}
print(image.count, "[\(minRow),\(maxRow)]", "[\(minCol),\(maxCol)]")
