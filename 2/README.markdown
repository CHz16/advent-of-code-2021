Day 2: Dive!
============

https://adventofcode.com/2021/day/2

Another easy warmup, just running through a list of instructions and doing some math along the way.
