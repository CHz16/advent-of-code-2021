//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "2", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var horizontal = 0, depth = 0
input.enumerateLines { line, stop in
    let components = line.components(separatedBy: " ")
    let distance = Int(components[1])!
    if components[0] == "forward" {
        horizontal += distance
    } else if components[0] == "up" {
        depth -= distance
    } else if components[0] == "down" {
        depth += distance
    }
}
print("\(horizontal) * \(depth) = \(horizontal * depth)")


// Part 2

print("--------------")

horizontal = 0
depth = 0
var aim = 0
input.enumerateLines { line, stop in
    let components = line.components(separatedBy: " ")
    let distance = Int(components[1])!
    if components[0] == "forward" {
        horizontal += distance
        depth += aim * distance
    } else if components[0] == "up" {
        aim -= distance
    } else if components[0] == "down" {
        aim += distance
    }
}
print("\(horizontal) * \(depth) = \(horizontal * depth)")
