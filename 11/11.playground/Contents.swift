//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "11", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var startingOctopi: [[Int]] = []
input.enumerateLines { line, stop in
    let numbers = line.compactMap { Int(String($0)) }
    startingOctopi.append(numbers)
}
let width = startingOctopi[0].count, height = startingOctopi.count

var octopi = startingOctopi
var i = 1
var flashes = 0
var synchronyTurn = -1
while synchronyTurn == -1 || i <= 100 {
    var flashesThisTurn = 0
    for row in 0..<height {
        for col in 0..<width {
            if octopi[row][col] == -1 {
                continue
            }
            octopi[row][col] += 1
            if octopi[row][col] > 9 {
                var flashStack = [(row, col)]
                while let (row, col) = flashStack.popLast() {
                    octopi[row][col] = -1
                    flashesThisTurn += 1
                    for (neighborRow, neighborCol) in [(row - 1, col - 1), (row, col - 1), (row + 1, col - 1), (row - 1, col), (row + 1, col), (row - 1, col + 1), (row, col + 1), (row + 1, col + 1)] {
                        if neighborRow < 0 || neighborRow >= height || neighborCol < 0 || neighborCol >= width {
                            continue
                        }
                        if octopi[neighborRow][neighborCol] == -1 || octopi[neighborRow][neighborCol] == 10 {
                            continue
                        }
                        octopi[neighborRow][neighborCol] += 1
                        if octopi[neighborRow][neighborCol] == 10 {
                            flashStack.append((neighborRow, neighborCol))
                        }
                    }
                }
            }
        }
    }
    for row in 0..<height {
        for col in 0..<height {
            if octopi[row][col] == -1 {
                octopi[row][col] = 0
            }
        }
    }

    if i <= 100 {
        flashes += flashesThisTurn
    }
    if synchronyTurn == -1 && flashesThisTurn == width * height {
        synchronyTurn = i
    }
    i += 1
}
print(octopi)
print(flashes)

// Part 2

print("--------------")

print(synchronyTurn)
