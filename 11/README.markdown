Day 11: Dumbo Octopus
=====================

https://adventofcode.com/2021/day/11

Struggled a bit to get this one actually working correctly, several bugs took some time to figure out in order to actually get the correct result from a time step. Absolutely nothing fancy anywhere in the code here, just straightforward implementation of the algorithm.

* Part 1: 1926th place (24:59)
* Part 2: 1744th place (27:19)
