Day 13: Transparent Origami
===========================

https://adventofcode.com/2021/day/13

The trick to part 1 is that you do not have to and absolutely should not actually keep track of the entire grid; all you have to do is maintain the list of currently active points. The folding step is to just iterate through the list of points, keeping the ones that are on the left/upper side of the fold because they don't move, and doing a little math to transform the ones on the right/lower side of the fold to what their folded coordinates are.

Part 2 is just output formatting
