//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "13", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

struct Point: Hashable { let x, y: Int }

var points: Set<Point> = Set()
var instructions: [(xAxis: Bool, coord: Int)] = []
var readingPoints = true
input.enumerateLines { line, stop in
    if line == "" {
        readingPoints = false
        return
    }
    if readingPoints {
        let coords = line.components(separatedBy: ",").compactMap { Int($0) }
        points.insert(Point(x: coords[0], y: coords[1]))
    } else {
        let components = line.components(separatedBy: " ")
        instructions.append((xAxis: components[0] == "x", coord: Int(components[1])!))
    }
}

var firstRun = true
for (xAxis, coord) in instructions {
    var newPoints: Set<Point> = Set()
    for point in points {
        if xAxis {
            if point.x < coord {
                newPoints.insert(point)
            } else {
                newPoints.insert(Point(x: 2 * coord - point.x, y: point.y))
            }
        } else {
            if point.y < coord {
                newPoints.insert(point)
            } else {
                newPoints.insert(Point(x: point.x, y: 2 * coord - point.y))
            }
        }
    }
    points = newPoints
    if firstRun {
        print(points.count)
        firstRun = false
    }
}


// Part 2

print("--------------")

var xMax = Int.min, yMax = Int.min
for point in points {
    xMax = max(xMax, point.x)
    yMax = max(yMax, point.y)
}

var chars = Array(repeating: Array(repeating: " ", count: xMax + 1), count: yMax + 1)
for point in points {
    chars[point.y][point.x] = "#"
}
for line in chars {
    print(line.joined())
}
