//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "12", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var smallCaves: Set<String> = Set()
var adjacents: [String: [String]] = [:]
input.enumerateLines { line, stop in
    let endpoints = line.components(separatedBy: "-")
    adjacents[endpoints[0], default: []].append(endpoints[1])
    adjacents[endpoints[1], default: []].append(endpoints[0])
    if endpoints[0].first!.isLowercase {
        smallCaves.insert(endpoints[0])
    }
    if endpoints[1].first!.isLowercase {
        smallCaves.insert(endpoints[1])
    }
}
smallCaves.subtract(["start", "end"])

struct State: Hashable { let cave: String, unvisitedSmallCaves: Set<String>, doubleVisited: Bool }
var memo: [State: Int] = [:]
func countPaths(from cave: String, unvisitedSmallCaves: Set<String>, doubleVisited: Bool) -> Int {
    let state = State(cave: cave, unvisitedSmallCaves: unvisitedSmallCaves, doubleVisited: doubleVisited)
    if let memoPaths = memo[state] {
        return memoPaths
    }

    var paths = 0
    for adjacent in adjacents[cave]! {
        if adjacent == "end" {
            paths += 1
            continue
        } else if adjacent == "start" {
            continue
        }
        let newDoubleVisited: Bool
        if adjacent.first!.isLowercase && !unvisitedSmallCaves.contains(adjacent) {
            if doubleVisited {
                continue
            } else {
                newDoubleVisited = true
            }
        } else {
            newDoubleVisited = doubleVisited
        }
        paths += countPaths(from: adjacent, unvisitedSmallCaves: unvisitedSmallCaves.subtracting([adjacent]), doubleVisited: newDoubleVisited)
    }

    memo[state] = paths
    return paths
}

print(countPaths(from: "start", unvisitedSmallCaves: smallCaves, doubleVisited: true))


// Part 2

print("--------------")

print(countPaths(from: "start", unvisitedSmallCaves: smallCaves, doubleVisited: false))
