Day 12: Passage Pathing
=======================

https://adventofcode.com/2021/day/12

For my first swing at this, I decided to just do a naive BFS that keeps track of paths explicitly, which is okay but causes a playground to cry because my answer is over 100,000. I ended up dropping down to a shell script for the first time in the contest in order to run it and get my answers.

However, it felt bad not playgrounding this one, so I decided afterward to try a memoized approach that just counts the number of paths, and that's so much better.
