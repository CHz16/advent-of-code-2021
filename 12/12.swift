#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "12.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

struct Path { let path: [String], unvisitedSmallCaves: Set<String>, doubleVisited: Bool }

var smallCaves: Set<String> = Set()
var adjacents: [String: [String]] = [:]
input.enumerateLines { line, stop in
    let endpoints = line.components(separatedBy: "-")
    adjacents[endpoints[0], default: []].append(endpoints[1])
    adjacents[endpoints[1], default: []].append(endpoints[0])
    if endpoints[0].first!.isLowercase {
        smallCaves.insert(endpoints[0])
    }
    if endpoints[1].first!.isLowercase {
        smallCaves.insert(endpoints[1])
    }
}
smallCaves.subtract(["start", "end"])

var queue = [Path(path: ["start"], unvisitedSmallCaves: smallCaves, doubleVisited: true)]
var paths = 0
while let path = queue.popLast() {
    for adjacent in adjacents[path.path.last!]! {
        if adjacent == "end" {
            paths += 1
            continue
        }
        let doubleVisited: Bool
        if adjacent.first!.isLowercase && !path.unvisitedSmallCaves.contains(adjacent) {
            if path.doubleVisited || adjacent == "start" {
                continue
            } else {
                doubleVisited = true
            }
        } else {
            doubleVisited = path.doubleVisited
        }
        queue.append(Path(path: path.path + [adjacent], unvisitedSmallCaves: path.unvisitedSmallCaves.subtracting([adjacent]), doubleVisited: doubleVisited))
    }
}
print(paths)


// Part 2

print("--------------")

queue = [Path(path: ["start"], unvisitedSmallCaves: smallCaves, doubleVisited: false)]
paths = 0
while let path = queue.popLast() {
    for adjacent in adjacents[path.path.last!]! {
        if adjacent == "end" {
            paths += 1
            continue
        }
        let doubleVisited: Bool
        if adjacent.first!.isLowercase && !path.unvisitedSmallCaves.contains(adjacent) {
            if path.doubleVisited || adjacent == "start" {
                continue
            } else {
                doubleVisited = true
            }
        } else {
            doubleVisited = path.doubleVisited
        }
        queue.append(Path(path: path.path + [adjacent], unvisitedSmallCaves: path.unvisitedSmallCaves.subtracting([adjacent]), doubleVisited: doubleVisited))
    }
}
print(paths)
