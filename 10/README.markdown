Day 10: Syntax Scoring
======================

https://adventofcode.com/2021/day/10

Checking nested brackets is a classic stack pattern: when you hit an opening bracket, push it onto the stack, and when you hit a closing bracket, pop an element from the stack and see if it matches. So part 1 is just doing that, and if we hit a mismatch, do the math

Part 2 also ends up being really easy with the stack: the remaining elements in the stack after we hit the end of the line are the chunks that are still open. So all we have to do there is just iterate over the stack and do the math

* Part 1: 203rd place (4:47)
* Part 2: 142nd place (8:40)
