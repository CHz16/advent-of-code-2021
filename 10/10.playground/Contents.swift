//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "10", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var scores: [Character: Int] = [
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137
]

var totalScore = 0
input.enumerateLines { line, stop in
    var stack: [Character] = []
    for character in line {
        if character == "(" {
            stack.append(")")
        } else if character == "[" {
            stack.append("]")
        } else if character == "{" {
            stack.append("}")
        } else if character == "<" {
            stack.append(">")
        } else {
            if stack.isEmpty || stack.popLast() != character {
                print(line, character)
                totalScore += scores[character]!
                return
            }
        }
    }
}
print(totalScore)


// Part 2

print("--------------")

scores = [
    ")": 1,
    "]": 2,
    "}": 3,
    ">": 4
]

var allScores: [Int] = []
input.enumerateLines { line, stop in
    var stack: [Character] = []
    for character in line {
        if character == "(" {
            stack.append(")")
        } else if character == "[" {
            stack.append("]")
        } else if character == "{" {
            stack.append("}")
        } else if character == "<" {
            stack.append(">")
        } else {
            if stack.isEmpty || stack.popLast() != character {
                return
            }
        }
    }

    var score = 0
    for character in stack.reversed() {
        score = score * 5 + scores[character]!
    }
    allScores.append(score)
    print(line, stack, score)
}
print(allScores.sorted()[allScores.count / 2])

