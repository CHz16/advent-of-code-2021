Day 24: Arithmetic Logic Unit
=============================

https://adventofcode.com/2021/day/24

And now it's the traditional "here's some mysterious low level code that you could implement, but it'll take a million years to run, so you need to figure out what it's doing enough to do it more efficiently" puzzle. The ALU is implemented in `24-1.swift` to try the provided microcode on the puzzle input, and unsurprisingly it'll never output anything.

So the microcode repeats the same operation 14 times: it calculates a value based on the next input `N`, three constants `A` `B` and `C`, and the previous value of the `Z` register, then stores the result in the `Z` register. The three constants are the numeric literals found on lines 5, 6, and 16 of the operation block, and they're different for each of the fourteen times you do the operation. The operation is as follows:

```
if Z % 26 + B != N:
    Z = Z / A * 26 + N + C
else:
    Z = Z / A
```

`A` is always either 1 or 26, `C` is always between 0 and 15 inclusive, and `N` is always a digit from 1 to 9, so `Z` will always be nonnegative. The sets of constants can be classified into two groups, each of which has seven members: ones where `A` = 1 and `B` >= 10, and ones where `A` = 26 and `B` < 0. Therefore, when we get a group of the first type, we'll always take the first branch, since `Z % 26 + B` will always be greater than or equal to 10, which is greater than `N`. These groups will multiply the previous value of `Z` by 26 and then add a number between 1 and 25.

With `B` negative, a group of the second type has the potential to take the second branch if the modulo condition is met. If the first branch is taken, the division and multiplication will round `Z` down to a multiple of 26 and then add another number between 1 and 25, keeping it around the same power of 26, while the second branch will divide `Z` by 26. This is the only way to reduce `Z`'s power of 26 by one, and since we're guaranteed to increase its power 7 times by each of the groups of the first type, we have to make sure that each of the groups of the second type take the second branch.

I didn't bother to figure out what the math was doing beyond that; instead I wrote a solution in `24-2.swift` that goes through every possible first digit and calculates the `Z` value and puts those into a list, then takes every number on the list and tries every possible second digit after that with the subsequent `Z` value and puts those into a list, and so on until we hit a decreasing step. Then, it does the same thing, but it only saves numbers that end up reducing `Z`'s power of 26, pruning the set of numbers we have to consider. It then just keeps going like that until we've run through 14 digits; everything remaining then is an answer.

As I've been writing this doc, I've figured out a better way to frame what the microcode does. A group of the first type is a push instruction: it pushes the number `N` + `C` onto the stack. A group of the second type pops a number `Q` from the stack and checks if `Q` + `B` = `N`, and if it's not, it pushes `N` + `C` onto the stack. This means that each digit in the number is paired with another, such that `D_m` + `C_m` + `B_n` = `D_n`. So we can just figure out which digits are related and directly calculate them. This is implemented in `24-3.swift`; for funsies, I decided to have it parse all the info out of the microcode for complete hands-off solving.

* Part 1: 267th place (1:29:21)
* Part 2: 210th place (1:29:46)
