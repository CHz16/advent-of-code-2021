#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "24.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Parts 1 & 2

let constants = [
    [1, 13, 0],
    [1, 11, 3],
    [1, 14, 8],
    [26, -5, 5],
    [1, 14, 13],
    [1, 10, 9],
    [1, 12, 6],
    [26, -14, 1],
    [26, -8, 1],
    [1, 13, 2],
    [26, 0, 7],
    [26, -5, 5],
    [26, -9, 8],
    [26, -1, 15]
]

func operate(input: Int, z: Int, iteration: Int) -> Int {
    let sub = (z % 26 + constants[iteration][1]) != input ? 1 : 0
    return z / constants[iteration][0] * (1 + 25 * sub) + (input + constants[iteration][2]) * sub
}

var zs = [(n: 0, z: 0)]
for i in 0..<14 {
    var newZs: [(n: Int, z: Int)] = []
    let decreasing = constants[i][0] == 26
    for (n, z) in zs {
        for digit in 1...9 {
            let newZ = operate(input: digit, z: z, iteration: i)
            let newN = n * 10 + digit
            if newZ <= (z / 26 * 26) {
                if i == 13 {
                    print(newN, z, "->", newZ)
                }
                newZs.append((n: newN, z: newZ))
            } else if !decreasing {
                newZs.append((n: newN, z: newZ))
            }
        }
    }
    zs = newZs
}
