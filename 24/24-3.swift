#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "24.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

var pairs: [(i1: Int, i2: Int, constant: Int)] = []
let lines = input.components(separatedBy: .newlines)
var stack: [(i: Int, constant: Int)] = []
for i in 0..<14 {
    let operationWords = lines[18 * i + 4].components(separatedBy: .whitespaces)
    if operationWords[2] == "1" {
        // push
        let constantWords = lines[18 * i + 15].components(separatedBy: .whitespaces)
        stack.append((i: i, constant: Int(constantWords[2])!))
    } else {
        // pop
        let constantWords = lines[18 * i + 5].components(separatedBy: .whitespaces)
        let (i1, constant) = stack.popLast()!
        pairs.append((i1: i1, i2: i, constant: constant + Int(constantWords[2])!))
    }
}

var n = Array(repeating: 0, count: 14)
for (i1, i2, constant) in pairs {
    if constant > 0 {
        n[i1] = 9 - constant
        n[i2] = 9
    } else {
        n[i1] = 9
        n[i2] = 9 + constant
    }
}
print(n.reduce(0) { 10 * $0 + $1 })


// Part 2

print("--------------")

n = Array(repeating: 0, count: 14)
for (i1, i2, constant) in pairs {
    if constant >= 0 {
        n[i1] = 1
        n[i2] = 1 + constant
    } else {
        n[i1] = 1 - constant
        n[i2] = 1
    }
}
print(n.reduce(0) { 10 * $0 + $1 })
