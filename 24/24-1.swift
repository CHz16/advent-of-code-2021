#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "24.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

func run(_ aluInput: String) -> [String: Int] {
    var aluInput = aluInput
    var variables: [String: Int] = ["w": 0, "x": 0, "y": 0, "z": 0]
    input.enumerateLines { line, stop in
        let components = line.components(separatedBy: " ")
        let a = components[1]
        let b: Int
        if components.count < 3 {
            b = -1
        } else if let value = Int(components[2]) {
            b = value
        } else {
            b = variables[components[2]]!
        }
        if components[0] == "inp" {
            let value = Int(String(aluInput[aluInput.startIndex]))!
            variables[a] = value
            _ = aluInput.remove(at: aluInput.startIndex)
        } else if components[0] == "add" {
            variables[a]! += b
        } else if components[0] == "mul" {
            variables[a]! *= b
        } else if components[0] == "div" {
            variables[a]! /= b
        } else if components[0] == "mod" {
            variables[a]! %= b
        } else if components[0] == "eql" {
            variables[a] = (variables[a]! == b ? 1 : 0)
        }
    }
    return variables
}

var i = 11111111111111
while true {
    let s = String(i)
    let z = run(s)["z"]!
    if z == 0  {
        print(s)
    }
    print(s, z)
    i += 1
}


// Part 2

print("--------------")
