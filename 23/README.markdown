Day 23: Amphipod
================

https://adventofcode.com/2021/day/23

While I started when the puzzle dropped today, I wasn't seriously trying for a leaderboard position and was just having fun implementing it "nicely" while doing some other stuff. And yet, I ended up with better positions than some days that I was actually trying on, so I guess I'll include my positions and times here

Instead of doing general pathfinding, this is very hardcoded to the specific structure of the puzzle, since the grid is always the same with the exception of starting amphipods and the room sizes. I did it as A* search, with the optimization of detecting certain types of deadlocks and pruning based on that. It still takes like 16 seconds or so to run (if you remove the line where it prints out every state checked), so it's not great great, but I'm satisfied

* Part 1: 4065th place (5:07:13)
* Part 2: 2067th place (5:24:22)
