#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "23-2.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Implementation from Introduction to Algorithms, Second Edition (CLRS)
struct PriorityQueue<ID: Hashable, Weight: Comparable> {

    typealias Entry = (id: ID, weight: Weight)

    var heap: [Entry] = []
    var count: Int
    var lookup: [ID: Int] = [:]

    init() {
        self.init(items: [])
    }

    init(items: [Entry]) {
        heap = items
        count = heap.count
        for (i, (id: id, weight: _)) in heap.enumerated() {
            lookup[id] = i
        }

        for i in (0..<(count / 2)).reversed() {
            minHeapify(root: i)
        }
    }

    var isEmpty: Bool {
        return count == 0
    }

    var min: Entry {
        return heap[0]
    }

    mutating func removeMin() -> Entry {
        return remove(i: 0)
    }

    mutating func remove(id: ID) -> Entry {
        return remove(i: lookup[id]!)
    }

    mutating func reduceWeight(of id: ID, to newWeight: Weight) {
        var i = lookup[id]!
        // This check could be <=, because if an entry is in the right place, then there's no need to do anything if we're setting the weight to the same value. However, the implementation of insertItem uses this method to force a heap rebalancing for the entry it inserts into potentially the *wrong* place, and so in that case it's useful to let it run.
        if heap[i].weight < newWeight {
            return
        }

        heap[i] = Entry(id: heap[i].id, weight: newWeight)
        while i > 0 && heap[parent(i)].weight > heap[i].weight {
            exchange(i, parent(i))
            i = parent(i)
        }
    }

    mutating func insertItem(id: ID, weight: Weight) {
        count += 1
        let newEntry = Entry(id: id, weight: weight)
        if heap.count < count {
            heap.append(newEntry)
        } else {
            heap[count - 1] = newEntry
        }
        lookup[newEntry.id] = count - 1
        reduceWeight(of: id, to: weight)
    }


    mutating func minHeapify(root i: Int) {
        let leftIndex = left(i)
        let rightIndex = right(i)

        var smallest: Int
        if leftIndex < count && heap[leftIndex].weight < heap[i].weight {
            smallest = leftIndex
        } else {
            smallest = i
        }
        if rightIndex < count && heap[rightIndex].weight < heap[smallest].weight {
            smallest = rightIndex
        }

        if smallest != i {
            exchange(smallest, i)
            minHeapify(root: smallest)
        }
    }

    mutating func remove(i: Int) -> Entry {
        let entry = heap[i]

        lookup[entry.id] = nil
        count -= 1
        if i < count {
            lookup[heap[count].id] = i
            heap[i] = heap[count]
            minHeapify(root: i)
        }

        return entry
    }

    func parent(_ i: Int) -> Int {
        return i / 2
    }

    func left(_ i: Int) -> Int {
        return 2 * i
    }

    func right(_ i: Int) -> Int {
        return 2 * i + 1
    }

    mutating func exchange(_ i1: Int, _ i2: Int) {
        lookup[heap[i1].id] = i2
        lookup[heap[i2].id] = i1
        (heap[i1], heap[i2]) = (heap[i2], heap[i1])
    }
}


// Parts 1 & 2

struct Point: Hashable, CustomStringConvertible {
    let x, y: Int
    var description: String { "(\(x),\(y))" }
}

struct Amphipod: Hashable, Comparable, CustomStringConvertible {
    enum Color: Int { case amber = 1, bronze = 10, copper = 100, desert = 1000 }

    let color: Color
    let position: Point

    init(colorCode: Character, position inPosition: Point) {
        if colorCode == "A" {
            color = .amber
        } else if colorCode == "B" {
            color = .bronze
        } else if colorCode == "C" {
            color = .copper
        } else {
            color = .desert
        }
        position = inPosition
    }

    init(color inColor: Color, position inPosition: Point) {
        color = inColor
        position = inPosition
    }

    func moved(to newPosition: Point) -> Amphipod {
        return Amphipod(color: color, position: newPosition)
    }

    var description: String {
        "\(color.rawValue)\(position)"
    }

    var destinationX: Int {
        switch color {
        case .amber:
            return 3
        case .bronze:
            return 5
        case .copper:
            return 7
        case .desert:
            return 9
        }
    }

    var bucket: Int {
        return (destinationX - 3) / 2
    }

    static func <(left: Amphipod, right: Amphipod) -> Bool {
        if left.position.y != right.position.y {
            return left.position.y < right.position.y
        }
        return left.position.x < right.position.x
    }
}

struct State: Hashable {
    let incorrectAmphipods: [Amphipod]
    let remaining, incorrect: [Int]

    var isWinState: Bool {
        return incorrectAmphipods.isEmpty
    }

    var estimatedFutureCost: Int {
        incorrectAmphipods.map { $0.color.rawValue * (abs($0.destinationX - $0.position.x) + $0.position.y - 1) }.reduce(0, +) + (remaining.reversed().reduce(0) { 10 * $0 + $1 * ($1 + 1) / 2 })
    }

    func makeFutureStates() -> [(state: State, cost: Int)] {
        let blockedSquares = Set(incorrectAmphipods.map { $0.position })
        var futureStates: [(state: State, cost: Int)] = []
        amphipod: for i in 0..<incorrectAmphipods.count {
            var newIncorrectAmphipods = incorrectAmphipods
            let amphipod = newIncorrectAmphipods.remove(at: i)
            if amphipod.position.y == 1 {
                // in hallway
                // room is not open - pass
                if incorrect[amphipod.bucket] > 0 {
                    continue amphipod
                }
                // room is open - check if path to room is open
                let delta = amphipod.destinationX > amphipod.position.x ? 1 : -1
                var x = amphipod.position.x
                repeat {
                    x += delta
                    if blockedSquares.contains(Point(x: x, y: 1)) {
                        continue amphipod
                    }
                } while x != amphipod.destinationX
                // path to room is open - calculate the cost to the bottommost open slot and update the remaining counter
                let cost = (abs(amphipod.destinationX - amphipod.position.x) + remaining[amphipod.bucket]) * amphipod.color.rawValue
                var newRemaining = remaining
                newRemaining[amphipod.bucket] -= 1
                let newState = State(incorrectAmphipods: newIncorrectAmphipods, remaining: newRemaining, incorrect: incorrect)
                futureStates.append((state: newState, cost: cost))
            } else {
                // not in hallway
                // check if path to hallway is open
                var y = amphipod.position.y
                repeat {
                    y -= 1
                    if blockedSquares.contains(Point(x: amphipod.position.x, y: y)) {
                        continue amphipod
                    }
                } while y != 1
                // from the room entrance, walk left until we hit something and add all those as states
                var newIncorrect = incorrect
                newIncorrect[(amphipod.position.x - 3) / 2] -= 1
                let upSteps = amphipod.position.y - 1
                var x = amphipod.position.x
                repeat {
                    x -= 1
                    let newPosition = Point(x: x, y: 1)
                    if blockedSquares.contains(newPosition) {
                        break
                    }
                    if x != 3 && x != 5 && x != 7 {
                        let newAmphipod = amphipod.moved(to: newPosition)
                        if deadlocked(amphipod: newAmphipod, blockers: newIncorrectAmphipods) {
                            continue
                        }
                        let cost = (upSteps + abs(x - amphipod.position.x)) * amphipod.color.rawValue
                        let newState = State(incorrectAmphipods: (newIncorrectAmphipods + [newAmphipod]).sorted(), remaining: remaining, incorrect: newIncorrect)
                        futureStates.append((state: newState, cost: cost))
                    }
                } while x > 1
                // from the room entrance, walk right until we hit something and add all those as states
                x = amphipod.position.x
                repeat {
                    x += 1
                    let newPosition = Point(x: x, y: 1)
                    if blockedSquares.contains(newPosition) {
                        break
                    }
                    if x != 5 && x != 7 && x != 9 {
                        let newAmphipod = amphipod.moved(to: newPosition)
                        if deadlocked(amphipod: newAmphipod, blockers: newIncorrectAmphipods) {
                            continue
                        }
                        let cost = (upSteps + abs(x - amphipod.position.x)) * amphipod.color.rawValue
                        let newState = State(incorrectAmphipods: (newIncorrectAmphipods + [newAmphipod]).sorted(), remaining: remaining, incorrect: newIncorrect)
                        futureStates.append((state: newState, cost: cost))
                    }
                } while x < 11
            }
        }

        return futureStates
    }
}

func deadlocked(amphipod: Amphipod, blockers: [Amphipod]) -> Bool {
    let deadlockers: [Amphipod]
    if amphipod.position.x < amphipod.destinationX {
        deadlockers = blockers.filter { $0.position.y == 1 && $0.destinationX < amphipod.position.x && $0.position.x > amphipod.position.x && $0.position.x < amphipod.destinationX }
    } else {
        deadlockers = blockers.filter { $0.position.y == 1 && $0.destinationX > amphipod.position.x && $0.position.x > amphipod.destinationX && $0.position.x < amphipod.position.x }
    }
    return !deadlockers.isEmpty
}


var incorrect = [0, 0, 0, 0]
var incorrectAmphipods: [Amphipod] = []
for (y, line) in (input.components(separatedBy: "\n").enumerated()).reversed() {
    for (x, char) in line.enumerated() {
        if char != "#" && char != " " && char != "." {
            let position = Point(x: x, y: y)
            if char != "." {
                let bucket = (x - 3) / 2
                let amphipod = Amphipod(colorCode: char, position: position)
                if incorrect[bucket] != 0 || amphipod.destinationX != x {
                    incorrect[bucket] = max(incorrect[bucket], y - 1)
                    incorrectAmphipods.append(amphipod)
                }
            }
        }
    }
}

let initialState = State(incorrectAmphipods: incorrectAmphipods.sorted(), remaining: incorrect, incorrect: incorrect)
var queue = PriorityQueue(items: [(id: initialState, weight: 0)])
var seen = Set([initialState]), visited: Set<State> = Set()
var costs = [initialState: 0]
while !queue.isEmpty {
    let (state, _) = queue.removeMin()
    if state.isWinState {
        print(state.incorrectAmphipods, costs[state]!)
        break
    }
    visited.insert(state)
    print(costs[state]!, state)

    for (newState, cost) in state.makeFutureStates() {
        if visited.contains(newState) {
            continue
        }
        let newCost = cost + costs[state]!
        if costs[newState, default: Int.max] < newCost {
            continue
        }
        costs[newState] = newCost
        let newWeight = newCost + newState.estimatedFutureCost
        if seen.contains(newState) {
            queue.reduceWeight(of: newState, to: newWeight)
        } else {
            seen.insert(newState)
            queue.insertItem(id: newState, weight: newWeight)
        }
    }
}
