//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "9", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var heightMap: [Int: [Int: Int]] = [:]
input.enumerateLines { line, stop in
    var row: [Int: Int] = [:]
    for (i, char) in line.enumerated() {
        row[i] = Int(String(char))!
    }
    heightMap[heightMap.count] = row
}

var totalRisk = 0
var lowPoints: [(Int, Int)] = []
for row in 0..<heightMap.count {
    col: for col in 0..<heightMap[0]!.count {
        for (rowDelta, colDelta) in [(-1, 0), (1, 0), (0, -1), (0, 1)] {
            if heightMap[row]![col]! >= heightMap[row + rowDelta, default: [:]][col + colDelta, default: Int.max] {
                continue col
            }
        }
        totalRisk += heightMap[row]![col]! + 1
        lowPoints.append((row, col))
    }
}
print(totalRisk)


// Part 2

print("--------------")

var seen: [Int: [Int: Bool]] = [:]
var basinSizes: [Int] = []
for sink in lowPoints {
    var basinSize = 0
    var stack = [sink]
    while let (row, col) = stack.popLast() {
        if seen[row, default: [:]][col, default: false] {
            continue
        }
        seen[row, default: [:]][col] = true
        basinSize += 1
        for (rowDelta, colDelta) in [(-1, 0), (1, 0), (0, -1), (0, 1)] {
            guard let neighborHeight = heightMap[row + rowDelta]?[col + colDelta] else {
                continue
            }
            if neighborHeight != 9 && neighborHeight > heightMap[row]![col]! {
                stack.append((row + rowDelta, col + colDelta))
            }
        }
    }
    basinSizes.append(basinSize)
}
print(basinSizes)
print(basinSizes.sorted { $0 > $1 }[0...2].reduce(1, *))
