Day 9: Smoke Basin
==================

https://adventofcode.com/2021/day/9

Part 1 is just going through a 2D grid and comparing elements with their neighbors. Part 2 then DFSes from the points we found in part 1 to find connected areas.

I started this one intending to compete, but I had to do something for a couple of minutes so that lost me some time and some ranks. Wouldn't have been a big difference though, 100th place was 10:31 and I definitely didn't spend ten minutes not working on this

* Part 1: 1201st place (8:36)
* Part 2: 785th place (20:29)
