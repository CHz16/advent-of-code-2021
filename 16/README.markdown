Day 16: Packet Decoder
======================

https://adventofcode.com/2021/day/16

Hey, I placed for the first and perhaps only times this year! I lost a few spots between parts 1 & 2 because I forgot to change the recursive calls in `readPacket2` whoopsie doopsie

So anyway, this is reading in an encoding of an expression tree and then evaluating said tree. The fact that an operator packet can have arbitrary packets as subpackets means that recursion is an excellent approach: have a function that reads in a packet from the bit stream, and when we find how many subpackets to read or how long to read them for, just call into the function again to evaluate the subpackets' values.

* Part 1: 53rd place (18:04)
* Part 2: 77th place (25:58)
