//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "16", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

let conversion: [Character: [Bool]] = [
    "0": [false, false, false, false],
    "1": [false, false, false, true],
    "2": [false, false, true, false],
    "3": [false, false, true, true],
    "4": [false, true, false, false],
    "5": [false, true, false, true],
    "6": [false, true, true, false],
    "7": [false, true, true, true],
    "8": [true, false, false, false],
    "9": [true, false, false, true],
    "A": [true, false, true, false],
    "B": [true, false, true, true],
    "C": [true, true, false, false],
    "D": [true, true, false, true],
    "E": [true, true, true, false],
    "F": [true, true, true, true]
]
var bits: [Bool] = []
for char in input {
    bits.append(contentsOf: conversion[char, default: []])
}

var pointer = 0

func readNumber(_ n: Int) -> Int {
    var result = 0
    for _ in 0..<n {
        result = result * 2 + (bits[pointer] ? 1 : 0)
        pointer += 1
    }
    return result
}

var versionTotal = 0
func readPacket() -> Int {
    let version = readNumber(3)
    versionTotal += version
    let type = readNumber(3)
    if type == 4 {
        var number = 0
        var lastPacket = false
        repeat {
            lastPacket = !bits[pointer]
            pointer += 1
            number = number * 16 + readNumber(4)
        } while !lastPacket
        return number
    } else {
        let lengthTypeID = readNumber(1)
        if lengthTypeID == 0 {
            let subpacketLength = readNumber(15)
            let ending = pointer + subpacketLength
            while pointer < ending {
                readPacket()
            }
        } else {
            let numberOfSubpackets = readNumber(11)
            for _ in 0..<numberOfSubpackets {
                readPacket()
            }
        }
    }

    return 0
}

while pointer < bits.count - 6 {
    readPacket()
}
print(versionTotal)


// Part 2

print("--------------")

func readPacket2() -> Int {
    let version = readNumber(3)
    versionTotal += version
    let type = readNumber(3)
    if type == 4 {
        var number = 0
        var lastPacket = false
        repeat {
            lastPacket = !bits[pointer]
            pointer += 1
            number = number * 16 + readNumber(4)
        } while !lastPacket
        return number
    } else {
        let lengthTypeID = readNumber(1)
        var subpackets: [Int] = []
        if lengthTypeID == 0 {
            let subpacketLength = readNumber(15)
            let ending = pointer + subpacketLength
            while pointer < ending {
                subpackets.append(readPacket2())
            }
        } else {
            let numberOfSubpackets = readNumber(11)
            for _ in 0..<numberOfSubpackets {
                subpackets.append(readPacket2())
            }
        }
        if type == 0 {
            return subpackets.reduce(0, +)
        } else if type == 1 {
            return subpackets.reduce(1, *)
        } else if type == 2 {
            return subpackets.min()!
        } else if type == 3 {
            return subpackets.max()!
        } else if type == 5 {
            return subpackets[0] > subpackets[1] ? 1 : 0
        } else if type == 6 {
            return subpackets[0] < subpackets[1] ? 1 : 0
        } else if type == 7 {
            return subpackets[0] == subpackets[1] ? 1 : 0
        }
    }

    return 0
}

pointer = 0
while pointer < bits.count - 6 {
    print(readPacket2())
}
