Day 17: Trick Shot
==================

https://adventofcode.com/2021/day/17

For some reason I had a really hard time getting this one to work, took several different rewrites on the approach and then a bunch of debugging after that for several errors. As you can see from the stats, my part 2 was a disaster; part 1 should've been worse, because it didn't actually work correctly at first, but it worked correctly *enough* to get the right answer.

The approach here is that I didn't want to brute force every single pair of `x` and `y` velocities, so instead the observation is that movement in both axes is independent, so we can figure them independently. For a given starting `x` velocity, there are certain time steps when any probe with that velocity will have an `x` coordinate in the target area, and the same is true for `y`. Therefore, a probe will land in the target area if the same time step is valid for both `x` and `y`.

To do this, we first run through every possible `x` velocity that doesn't completely overshoot the target area, find the valid time steps for it, and compile a dictionary indexed by time step: `dict[t]` will return all `x` velocities that would reach the target in that time. Then, we go through every possible `y` velocity that doesn't completely overshoot the target area, find the valid time steps for it, and then check if there are any valid `x` velocities for those time steps in the previous dictionary.

There's some nuance here to avoid double counting and because some starting `x` velocities will come to a dead stop in the target area, meaning *any* time step past when it stops is valid. Both of those problems led to the high part 2 time.

* Part 1: 1185th place (22:18)
* Part 2: 3745th place (1:16:23)
