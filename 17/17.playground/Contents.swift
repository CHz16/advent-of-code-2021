//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "17", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Parts 1 & 2

let coords = input.components(separatedBy: .whitespacesAndNewlines).compactMap { Int($0) }

var validVelocitiesForTime: [Int: [Int]] = [:]
var xVelocity = 1
var deadStops: [Int] = []
while xVelocity <= coords[1] {
    var currentXVelocity = xVelocity, x = 0, t = 0
    while currentXVelocity > 0 {
        x += currentXVelocity
        currentXVelocity -= 1
        t += 1
        if x > coords[1] {
            break
        }
        if x >= coords[0] && x <= coords[1] {
            validVelocitiesForTime[t, default: []].append(xVelocity)
        }

    }
    if currentXVelocity == 0 && x >= coords[0] && x <= coords[1] {
        deadStops.append(xVelocity)
    }
    xVelocity += 1
}
for t in validVelocitiesForTime.keys {
    validVelocitiesForTime[t]!.append(contentsOf: deadStops.filter { t > $0 } )
}

var highestY = 0
var solutions = 0
for yVelocity in (coords[2])...(-coords[2]) {
    var currentYVelocity = yVelocity, y = 0, t = 0
    var xVelocities: Set<Int> = Set()
    while y >= coords[2] {
        t += 1
        y += currentYVelocity
        currentYVelocity -= 1
        if y >= coords[2] && y <= coords[3] {
            if let velocities = validVelocitiesForTime[t] {
                xVelocities.formUnion(velocities)
            } else if t >= deadStops.min()! {
                xVelocities.formUnion(deadStops)
            } else {
                continue
            }
            let runHighestY = yVelocity > 0 ? yVelocity * (yVelocity + 1) / 2 : 0
            if runHighestY > highestY {
                highestY = runHighestY
                print(yVelocity, runHighestY)
            }
        }
    }
    solutions += xVelocities.count
}
print("--------------")
print(solutions)

