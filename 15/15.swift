#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "n.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

struct Point: Equatable { let row, col: Int }

var grid: [[Int]] = []
input.enumerateLines { line, stop in
    grid.append(line.compactMap { Int(String($0)) })
}
var height = grid.count, width = grid[0].count

var visited = Array(repeating: Array(repeating: false, count: width), count: height)
var distances = Array(repeating: Array(repeating: Int.max, count: width), count: height)

distances[0][0] = 0
var pending = [Point(row: 0, col: 0)]
while let point = pending.popLast() {
    let row = point.row, col = point.col
    if row == height - 1 && col == width - 1 {
        print(distances[row][col])
        break
    }

    visited[row][col] = true
    var neighbors: [Point] = []
    if row > 0 && !visited[row - 1][col] {
        neighbors.append(Point(row: row - 1, col: col))
    }
    if row < height - 1 && !visited[row + 1][col] {
        neighbors.append(Point(row: row + 1, col: col))
    }
    if col > 0 && !visited[row][col - 1] {
        neighbors.append(Point(row: row, col: col - 1))
    }
    if col < width - 1 && !visited[row][col + 1] {
        neighbors.append(Point(row: row, col: col + 1))
    }
    for neighbor in neighbors {
        distances[neighbor.row][neighbor.col] = min(distances[neighbor.row][neighbor.col], distances[row][col] + grid[neighbor.row][neighbor.col])
        if !pending.contains(neighbor) {
            pending.append(neighbor)
        }
    }
    pending.sort { distances[$0.row][$0.col] > distances[$1.row][$1.col] }
}


// Part 2

print("--------------")

for i in 0..<height {
    var row = grid[i]
    for _ in 0..<4 {
        row = row.map { $0 < 9 ? $0 + 1 : 1 }
        grid[i].append(contentsOf: row)
    }
}
for x in 0..<4 {
    for i in 0..<height {
        grid.append(grid[x * height + i].map { $0 < 9 ? $0 + 1 : 1 })
    }
}
height *= 5
width *= 5


visited = Array(repeating: Array(repeating: false, count: width), count: height)
distances = Array(repeating: Array(repeating: Int.max, count: width), count: height)

distances[0][0] = 0
pending = [Point(row: 0, col: 0)]
while let point = pending.popLast() {
    let row = point.row, col = point.col
    if row == height - 1 && col == width - 1 {
        print(distances[row][col])
        break
    }

    visited[row][col] = true
    var neighbors: [Point] = []
    if row > 0 && !visited[row - 1][col] {
        neighbors.append(Point(row: row - 1, col: col))
    }
    if row < height - 1 && !visited[row + 1][col] {
        neighbors.append(Point(row: row + 1, col: col))
    }
    if col > 0 && !visited[row][col - 1] {
        neighbors.append(Point(row: row, col: col - 1))
    }
    if col < width - 1 && !visited[row][col + 1] {
        neighbors.append(Point(row: row, col: col + 1))
    }
    for neighbor in neighbors {
        distances[neighbor.row][neighbor.col] = min(distances[neighbor.row][neighbor.col], distances[row][col] + grid[neighbor.row][neighbor.col])
        if !pending.contains(neighbor) {
            pending.append(neighbor)
        }
    }
    pending.sort { distances[$0.row][$0.col] > distances[$1.row][$1.col] }
}
