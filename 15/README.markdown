Day 15: Chiton
==============

https://adventofcode.com/2021/day/15

I had this grand hope to have a solution to every puzzle this year run in a Swift playground in a "reasonable amount of time" (vaguely pinned around 10 seconds), but now here comes finding the shortest cost path through a graph with 250,000 nodes, so that plan's out the window. Just a bog standard implementation of Dijkstra's algorithm here.

The original solution runs like garbage on part 2, which is to be expected given the way it sorts the frontier array on every iteration, so in `15-2.swift` I copied in the priority queue implementation I wrote for a completely different project years ago and it cuts the execution time down significantly.

* Part 1: 1458th place (24:31)
* Part 2: 1110th place (39:56)
