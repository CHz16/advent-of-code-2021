#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "15.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)

// Implementation from Introduction to Algorithms, Second Edition (CLRS)
struct PriorityQueue<ID: Hashable, Weight: Comparable> {

    typealias Entry = (id: ID, weight: Weight)

    var heap: [Entry] = []
    var count: Int
    var lookup: [ID: Int] = [:]

    init() {
        self.init(items: [])
    }

    init(items: [Entry]) {
        heap = items
        count = heap.count
        for (i, (id: id, weight: _)) in heap.enumerated() {
            lookup[id] = i
        }

        for i in (0..<(count / 2)).reversed() {
            minHeapify(root: i)
        }
    }

    var isEmpty: Bool {
        return count == 0
    }

    var min: Entry {
        return heap[0]
    }

    mutating func removeMin() -> Entry {
        return remove(i: 0)
    }

    mutating func remove(id: ID) -> Entry {
        return remove(i: lookup[id]!)
    }

    mutating func reduceWeight(of id: ID, to newWeight: Weight) {
        var i = lookup[id]!
        // This check could be <=, because if an entry is in the right place, then there's no need to do anything if we're setting the weight to the same value. However, the implementation of insertItem uses this method to force a heap rebalancing for the entry it inserts into potentially the *wrong* place, and so in that case it's useful to let it run.
        if heap[i].weight < newWeight {
            return
        }

        heap[i] = Entry(id: heap[i].id, weight: newWeight)
        while i > 0 && heap[parent(i)].weight > heap[i].weight {
            exchange(i, parent(i))
            i = parent(i)
        }
    }

    mutating func insertItem(id: ID, weight: Weight) {
        count += 1
        let newEntry = Entry(id: id, weight: weight)
        if heap.count < count {
            heap.append(newEntry)
        } else {
            heap[count - 1] = newEntry
        }
        lookup[newEntry.id] = count - 1
        reduceWeight(of: id, to: weight)
    }


    mutating func minHeapify(root i: Int) {
        let leftIndex = left(i)
        let rightIndex = right(i)

        var smallest: Int
        if leftIndex < count && heap[leftIndex].weight < heap[i].weight {
            smallest = leftIndex
        } else {
            smallest = i
        }
        if rightIndex < count && heap[rightIndex].weight < heap[smallest].weight {
            smallest = rightIndex
        }

        if smallest != i {
            exchange(smallest, i)
            minHeapify(root: smallest)
        }
    }

    mutating func remove(i: Int) -> Entry {
        let entry = heap[i]

        lookup[entry.id] = nil
        count -= 1
        if i < count {
            lookup[heap[count].id] = i
            heap[i] = heap[count]
            minHeapify(root: i)
        }

        return entry
    }

    func parent(_ i: Int) -> Int {
        return i / 2
    }

    func left(_ i: Int) -> Int {
        return 2 * i
    }

    func right(_ i: Int) -> Int {
        return 2 * i + 1
    }

    mutating func exchange(_ i1: Int, _ i2: Int) {
        lookup[heap[i1].id] = i2
        lookup[heap[i2].id] = i1
        (heap[i1], heap[i2]) = (heap[i2], heap[i1])
    }
}


// Part 1

struct Point: Equatable, Hashable { let row, col: Int }

var grid: [[Int]] = []
input.enumerateLines { line, stop in
    grid.append(line.compactMap { Int(String($0)) })
}
var height = grid.count, width = grid[0].count

var visited = Array(repeating: Array(repeating: false, count: width), count: height)
var distances = Array(repeating: Array(repeating: Int.max, count: width), count: height)

distances[0][0] = 0
var pending = PriorityQueue(items: [(id: Point(row: 0, col: 0), weight: 0)])
while true {
    let point = pending.removeMin()
    let row = point.id.row, col = point.id.col
    if row == height - 1 && col == width - 1 {
        print(distances[row][col])
        break
    }

    visited[row][col] = true
    var neighbors: [Point] = []
    if row > 0 && !visited[row - 1][col] {
        neighbors.append(Point(row: row - 1, col: col))
    }
    if row < height - 1 && !visited[row + 1][col] {
        neighbors.append(Point(row: row + 1, col: col))
    }
    if col > 0 && !visited[row][col - 1] {
        neighbors.append(Point(row: row, col: col - 1))
    }
    if col < width - 1 && !visited[row][col + 1] {
        neighbors.append(Point(row: row, col: col + 1))
    }
    for neighbor in neighbors {
        let newWeight = min(distances[neighbor.row][neighbor.col], distances[row][col] + grid[neighbor.row][neighbor.col])
        let seenBefore = (distances[neighbor.row][neighbor.col] != Int.max)
        distances[neighbor.row][neighbor.col] = newWeight
        if seenBefore {
            pending.reduceWeight(of: neighbor, to: newWeight)
        } else {
            pending.insertItem(id: neighbor, weight: newWeight)
        }
    }
}


// Part 2



print("--------------")

for i in 0..<height {
    var row = grid[i]
    for _ in 0..<4 {
        row = row.map { $0 < 9 ? $0 + 1 : 1 }
        grid[i].append(contentsOf: row)
    }
}
for x in 0..<4 {
    for i in 0..<height {
        grid.append(grid[x * height + i].map { $0 < 9 ? $0 + 1 : 1 })
    }
}
height *= 5
width *= 5


visited = Array(repeating: Array(repeating: false, count: width), count: height)
distances = Array(repeating: Array(repeating: Int.max, count: width), count: height)

distances[0][0] = 0
pending = PriorityQueue(items: [(id: Point(row: 0, col: 0), weight: 0)])
while true {
    let point = pending.removeMin()
    let row = point.id.row, col = point.id.col
    if row == height - 1 && col == width - 1 {
        print(distances[row][col])
        break
    }

    visited[row][col] = true
    var neighbors: [Point] = []
    if row > 0 && !visited[row - 1][col] {
        neighbors.append(Point(row: row - 1, col: col))
    }
    if row < height - 1 && !visited[row + 1][col] {
        neighbors.append(Point(row: row + 1, col: col))
    }
    if col > 0 && !visited[row][col - 1] {
        neighbors.append(Point(row: row, col: col - 1))
    }
    if col < width - 1 && !visited[row][col + 1] {
        neighbors.append(Point(row: row, col: col + 1))
    }
    for neighbor in neighbors {
        let newWeight = min(distances[neighbor.row][neighbor.col], distances[row][col] + grid[neighbor.row][neighbor.col])
        let seenBefore = (distances[neighbor.row][neighbor.col] != Int.max)
        distances[neighbor.row][neighbor.col] = newWeight
        if seenBefore {
            pending.reduceWeight(of: neighbor, to: newWeight)
        } else {
            pending.insertItem(id: neighbor, weight: newWeight)
        }
    }
}
