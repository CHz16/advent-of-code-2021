//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "test1", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

print(input)


// Part 2

print("--------------")
