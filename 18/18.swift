#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "18.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

class Pair: CustomStringConvertible {
    var left, right: Pair?
    weak var parent: Pair?
    var value: Int?

    var description: String {
        if let value = value {
            return String(value)
        } else if let left = left, let right = right {
            return "[\(left),\(right)]"
        } else {
            return "[]"
        }
    }

    func findLeftValue() -> Pair? {
        var currentPair = self
        while true {
            guard let nextPair = currentPair.parent else {
                return nil
            }
            if nextPair.left! === currentPair {
                currentPair = nextPair
                continue
            } else {
                currentPair = nextPair
                break
            }
        }
        currentPair = currentPair.left!
        while currentPair.value == nil {
            currentPair = currentPair.right!
        }
        return currentPair
    }

    func findRightValue() -> Pair? {
        var currentPair = self
        while true {
            guard let nextPair = currentPair.parent else {
                return nil
            }
            if nextPair.right! === currentPair {
                currentPair = nextPair
                continue
            } else {
                currentPair = nextPair
                break
            }
        }
        currentPair = currentPair.right!
        while currentPair.value == nil {
            currentPair = currentPair.left!
        }
        return currentPair
    }
}

func readPair(_ s: String) -> Pair {
    func readPair(from s: String, at index: String.Index) -> (pair: Pair, nextIndex: String.Index) {
        if let n = Int(String(s[index])) {
            let pair = Pair()
            pair.value = n
            return (pair: pair, nextIndex: s.index(after: index))
        } else {
            let (left, rightIndex) = readPair(from: s, at: s.index(after: index))
            let (right, endIndex) = readPair(from: s, at: s.index(after: rightIndex))
            let pair = Pair()
            pair.left = left
            left.parent = pair
            pair.right = right
            right.parent = pair
            return (pair: pair, nextIndex: s.index(after: endIndex))
        }
    }
    return readPair(from: s, at: s.startIndex).pair
}

func reducePair(_ pair: Pair) {
    func findExplodingPair(_ pair: Pair, depth: Int = 0) -> Pair? {
        if pair.value != nil {
            return nil
        } else if let left = pair.left, let right = pair.right {
            if depth == 4 {
                return pair
            } else {
                return findExplodingPair(left, depth: depth + 1) ?? findExplodingPair(right, depth: depth + 1)
            }
        }
        return nil
    }

    func findSplittingPair(_ pair: Pair) -> Pair? {
        if let value = pair.value {
            if value > 9 {
                return pair
            } else {
                return nil
            }
        } else {
            return findSplittingPair(pair.left!) ?? findSplittingPair(pair.right!)
        }
    }

    var reduced: Bool
    repeat {
        reduced = false
        if let explodingPair = findExplodingPair(pair) {
            if let leftValue = explodingPair.findLeftValue() {
                leftValue.value! += explodingPair.left!.value!
            }
            if let rightValue = explodingPair.findRightValue() {
                rightValue.value! += explodingPair.right!.value!
            }
            let newPair = Pair()
            newPair.value = 0
            newPair.parent = explodingPair.parent!
            if explodingPair.parent!.left === explodingPair {
                explodingPair.parent!.left = newPair
            } else {
                explodingPair.parent!.right = newPair
            }
            reduced = true
        } else if let splittingPair = findSplittingPair(pair) {
            let newPair = Pair()
            newPair.parent = splittingPair.parent
            let leftValue = Pair()
            leftValue.value = splittingPair.value! / 2
            leftValue.parent = newPair
            newPair.left = leftValue
            let rightValue = Pair()
            rightValue.value = splittingPair.value! / 2 + (splittingPair.value! % 2 == 1 ? 1 : 0)
            rightValue.parent = newPair
            newPair.right = rightValue
            if splittingPair.parent!.left === splittingPair {
                splittingPair.parent!.left = newPair
            } else {
                splittingPair.parent!.right = newPair
            }
            reduced = true
        }
    } while reduced
}

func magnitude(_ pair: Pair) -> Int {
    if let value = pair.value {
        return value
    } else {
        return 3 * magnitude(pair.left!) + 2 * magnitude(pair.right!)
    }
}


let lines = input.components(separatedBy: "\n").dropLast()

var currentPair = readPair(lines[0])
print(currentPair)
for line in lines {
    let rightPair = readPair(line)
    let sumPair = Pair()
    sumPair.left = currentPair
    currentPair.parent = sumPair
    sumPair.right = rightPair
    rightPair.parent = sumPair
    reducePair(sumPair)
    currentPair = sumPair
    print(currentPair)
}
print(magnitude(currentPair))


// Part 2

print("--------------")

var maxPairwiseMagnitude = Int.min
for i in 0..<(lines.count - 1) {
    for j in (i + 1)..<lines.count {
        let sumPair1 = Pair()
        let leftPair1 = readPair(lines[i])
        let rightPair1 = readPair(lines[j])
        sumPair1.left = leftPair1
        leftPair1.parent = sumPair1
        sumPair1.right = rightPair1
        rightPair1.parent = sumPair1
        reducePair(sumPair1)
        let magnitude1 = magnitude(sumPair1)
        if magnitude1 > maxPairwiseMagnitude {
            print(magnitude1)
            maxPairwiseMagnitude = magnitude1
        }

        let sumPair2 = Pair()
        let leftPair2 = readPair(lines[j])
        let rightPair2 = readPair(lines[i])
        sumPair2.left = leftPair2
        leftPair2.parent = sumPair2
        sumPair2.right = rightPair2
        rightPair2.parent = sumPair2
        reducePair(sumPair2)
        let magnitude2 = magnitude(sumPair2)
        if magnitude2 > maxPairwiseMagnitude {
            print(magnitude2)
            maxPairwiseMagnitude = magnitude2
        }
    }
}
