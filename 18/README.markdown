Day 18: Snailfish
=================

https://adventofcode.com/2021/day/18

Some nontrivial binary tree operations today. For some reason I decided that I really wanted to do a true binary tree data structure in Swift, so that's what we did. Runs like butt in a playground for reasons I didn't feel like debugging, may come back later and try a playground optimized version but probably not

* Part 1: 1199th place (1:39:36)
* Part 2: 1215th place (1:49:43)
