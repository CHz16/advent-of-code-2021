Day 4: Giant Squid
==================

https://adventofcode.com/2021/day/4

I changed the data representation I used to track boards and marks twice while coding this, so my overall performance was not great. Part 2 is just a copy/paste of part 1 and doing it more than once.

* Part 1: 2063rd place (26:04)
* Part 2: 1733rd place (32:16)
