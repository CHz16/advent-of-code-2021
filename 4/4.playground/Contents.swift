//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "4", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

let winChecks = [
    [(row: 0, col: 0), (row: 0, col: 1), (row: 0, col: 2), (row: 0, col: 3), (row: 0, col: 4)],
    [(row: 1, col: 0), (row: 1, col: 1), (row: 1, col: 2), (row: 1, col: 3), (row: 1, col: 4)],
    [(row: 2, col: 0), (row: 2, col: 1), (row: 2, col: 2), (row: 2, col: 3), (row: 2, col: 4)],
    [(row: 3, col: 0), (row: 3, col: 1), (row: 3, col: 2), (row: 3, col: 3), (row: 3, col: 4)],
    [(row: 4, col: 0), (row: 4, col: 1), (row: 4, col: 2), (row: 4, col: 3), (row: 4, col: 4)],
    [(row: 0, col: 0), (row: 1, col: 0), (row: 2, col: 0), (row: 3, col: 0), (row: 4, col: 0)],
    [(row: 0, col: 1), (row: 1, col: 1), (row: 2, col: 1), (row: 3, col: 1), (row: 4, col: 1)],
    [(row: 0, col: 2), (row: 1, col: 2), (row: 2, col: 2), (row: 3, col: 2), (row: 4, col: 2)],
    [(row: 0, col: 3), (row: 1, col: 3), (row: 2, col: 3), (row: 3, col: 3), (row: 4, col: 3)],
    [(row: 0, col: 4), (row: 1, col: 4), (row: 2, col: 4), (row: 3, col: 4), (row: 4, col: 4)],
    //[(row: 0, col: 0), (row: 1, col: 1), (row: 2, col: 2), (row: 3, col: 3), (row: 4, col: 4)],
    //[(row: 0, col: 4), (row: 1, col: 3), (row: 2, col: 2), (row: 3, col: 1), (row: 4, col: 0)]
]
func checkWin(_ marks: [Int: [Int: Bool]]) -> Bool {
    outer: for winCheck in winChecks {
        for (row, col) in winCheck {
            if !marks[row, default: [:]][col, default: false] {
                continue outer
            }
        }
        return true
    }
    return false
}


var lines = input.components(separatedBy: "\n")
let draw = lines[0].components(separatedBy: ",").compactMap { Int($0) }
lines.removeFirst(2)
let numberOfBoards = lines.count / 6

var boards: [[Int: [Int: Int]]] = []
var slots: [Int: [(i: Int, row: Int, col: Int)]] = [:]
for i in 0..<numberOfBoards {
    var board: [Int: [Int: Int]] = [:]
    for row in 0..<5 {
        let numbers = lines[row].components(separatedBy: " ").compactMap { Int($0) }
        for col in 0..<5 {
            slots[numbers[col], default: []].append((i: i, row: row, col: col))
            board[row, default: [:]][col] = numbers[col]
        }
    }
    boards.append(board)
    lines.removeFirst(6)
}

var marks: [[Int: [Int: Bool]]] = Array(repeating: [:], count: numberOfBoards)
outer: for n in draw {
    for (i, row, col) in slots[n, default: []] {
        marks[i][row, default: [:]][col] = true
    }
    for i in 0..<numberOfBoards {
        if checkWin(marks[i]) {
            print(marks[i])
            print(boards[i])
            var unmarkedSum = 0
            for row in 0..<5 {
                for col in 0..<5 {
                    if !marks[i][row, default: [:]][col, default: false] {
                        unmarkedSum += boards[i][row]![col]!
                    }
                }
            }
            print("\(unmarkedSum) * \(n) = \(unmarkedSum * n)")
            break outer
        }
    }
}


// Part 2

print("--------------")

marks = Array(repeating: [:], count: numberOfBoards)
var alive = Array(0..<numberOfBoards)
outer: for n in draw {
    for (i, row, col) in slots[n, default: []] {
        if alive.contains(i) {
            marks[i][row, default: [:]][col] = true
        }
    }
    for (index, i) in alive.enumerated().reversed() {
        if checkWin(marks[i]) {
            print("\(i) wins")
            if alive.count > 1 {
                alive.remove(at: index)
                continue
            }
            print(marks[i])
            print(boards[i])
            var unmarkedSum = 0
            for row in 0..<5 {
                for col in 0..<5 {
                    if !marks[i][row, default: [:]][col, default: false] {
                        unmarkedSum += boards[i][row]![col]!
                    }
                }
            }
            print("\(unmarkedSum) * \(n) = \(unmarkedSum * n)")
            break outer
        }
    }
}
