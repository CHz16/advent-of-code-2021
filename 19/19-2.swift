#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "19.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

struct Vector3: Hashable, Comparable, Equatable, CustomStringConvertible {
    let x, y, z: Int

    var description: String { "(\(x), \(y), \(z))" }

    func rotated(_ orientation: Int) -> Vector3 {
        switch orientation {
        case 0:
            // 0x 0y 0z
            return self
        case 1:
            // 0x 0y 1z
            return Vector3(x: -y, y: x, z: z)
        case 2:
            // 0x 0y 2z
            return Vector3(x: -x, y: -y, z: z)
        case 3:
            // 0x 0y 3z
            return Vector3(x: y, y: -x, z: z)
        case 4:
            // 0x 1y 0z
            return Vector3(x: -z, y: y, z: x)
        case 5:
            // 0x 1y 1z
            return Vector3(x: -y, y: -z, z: x)
        case 6:
            // 0x 1y 2z
            return Vector3(x: z, y: -y, z: x)
        case 7:
            // 0x 1y 3z
            return Vector3(x: y, y: z, z: x)
        case 8:
            // 0x 2y 0z
            return Vector3(x: -x, y: y, z: -z)
        case 9:
            // 0x 2y 1z
            return Vector3(x: -y, y: -x, z: -z)
        case 10:
            // 0x 2y 2z
            return Vector3(x: x, y: -y, z: -z)
        case 11:
            // 0x 2y 3z
            return Vector3(x: y, y: x, z: -z)
        case 12:
            // 0x 3y 0z
            return Vector3(x: z, y: y, z: -x)
        case 13:
            // 0x 3y 1z
            return Vector3(x: -y, y: z, z: -x)
        case 14:
            // 0x 3y 2z
            return Vector3(x: -z, y: -y, z: -x)
        case 15:
            // 0x 3y 3z
            return Vector3(x: y, y: -z, z: -x)
        case 16:
            // 1x 0y 0z
            return Vector3(x: x, y: -z, z: y)
        case 17:
            // 1x 0y 1z
            return Vector3(x: z, y: x, z: y)
        case 18:
            // 1x 0y 2z
            return Vector3(x: -x, y: z, z: y)
        case 19:
            // 1x 0y 3z
            return Vector3(x: -z, y: -x, z: y)
        case 20:
            // 1x 1y 0z
            //return Vector3(x: -y, y: -z, z: x)
            // 1x 1y 1z
            //return Vector3(x: z, y: -y, z: x)
            // 1x 1y 2z
            //return Vector3(x: y, y: z, z: x)
            // 1x 1y 3z
            //return Vector3(x: -z, y: y, z: x)
            // 1x 2y 0z
            return Vector3(x: -x, y: -z, z: -y)
        case 21:
            // 1x 2y 1z
            return Vector3(x: z, y: -x, z: -y)
        case 22:
            // 1x 2y 2z
            return Vector3(x: x, y: z, z: -y)
        case 23:
            // 1x 2y 3z
            return Vector3(x: -z, y: x, z: -y)
        default:
            // 1x 3y 0z
            //return Vector3(x: y, y: -z, z: -x)
            // 1x 3y 1z
            //return Vector3(x: z, y: y, z: -x)
            // 1x 3y 2z
            //return Vector3(x: -y, y: z, z: -x)
            // 1x 3y 3z
            //return Vector3(x: -z, y: -y, z: -x)
            // 2x 0y 0z
            //return Vector3(x: x, y: -y, z: -z)
            // 2x 0y 1z
            //return Vector3(x: y, y: x, z: -z)
            // 2x 0y 2z
            //return Vector3(x: -x, y: y, z: -z)
            // 2x 0y 3z
            //return Vector3(x: -y, y: -x, z: -z)
            // 2x 1y 0z
            //return Vector3(x: z, y: -y, z: x)
            // 2x 1y 1z
            //return Vector3(x: y, y: z, z: x)
            // 2x 1y 2z
            //return Vector3(x: -z, y: y, z: x)
            // 2x 1y 3z
            //return Vector3(x: -y, y: -z, z: x)
            // 2x 2y 0z
            //return Vector3(x: -x, y: -y, z: z)
            // 2x 2y 1z
            //return Vector3(x: y, y: -x, z: z)
            // 2x 2y 2z
            //return Vector3(x: x, y: y, z: z)
            // 2x 2y 3z
            //return Vector3(x: -y, y: x, z: z)
            // 2x 3y 0z
            //return Vector3(x: -z, y: -y, z: -x)
            // 2x 3y 1z
            //return Vector3(x: y, y: -z, z: -x)
            // 2x 3y 2z
            //return Vector3(x: z, y: y, z: -x)
            // 2x 3y 3z
            //return Vector3(x: -y, y: z, z: -x)
            // 3x 0y 0z
            //return Vector3(x: x, y: z, z: -y)
            // 3x 0y 1z
            //return Vector3(x: -z, y: x, z: -y)
            // 3x 0y 2z
            //return Vector3(x: -x, y: -z, z: -y)
            // 3x 0y 3z
            //return Vector3(x: z, y: -x, z: -y)
            // 3x 1y 0z
            //return Vector3(x: y, y: z, z: x)
            // 3x 1y 1z
            //return Vector3(x: -z, y: y, z: x)
            // 3x 1y 2z
            //return Vector3(x: -y, y: -z, z: x)
            // 3x 1y 3z
            //return Vector3(x: z, y: -y, z: x)
            // 3x 2y 0z
            //return Vector3(x: -x, y: z, z: y)
            // 3x 2y 1z
            //return Vector3(x: -z, y: -x, z: y)
            // 3x 2y 2z
            //return Vector3(x: x, y: -z, z: y)
            // 3x 2y 3z
            //return Vector3(x: z, y: x, z: y)
            // 3x 3y 0z
            //return Vector3(x: -y, y: z, z: -x)
            // 3x 3y 1z
            //return Vector3(x: -z, y: -y, z: -x)
            // 3x 3y 2z
            //return Vector3(x: y, y: -z, z: -x)
            // 3x 3y 3z
            //return Vector3(x: z, y: y, z: -x)
            return self
        }
    }

    static func +(left: Vector3, right: Vector3) -> Vector3 {
        return Vector3(x: left.x + right.x, y: left.y + right.y, z: left.z + right.z)
    }

    static func -(left: Vector3, right: Vector3) -> Vector3 {
        return Vector3(x: left.x - right.x, y: left.y - right.y, z: left.z - right.z)
    }

    static func <(left: Vector3, right: Vector3) -> Bool {
        if left.x < right.x {
            return true
        } else if left.x > right.x {
            return false
        }
        if left.y < right.y {
            return true
        } else if left.y > right.y {
            return false
        }
        if left.z < right.z {
            return true
        } else if left.z > right.z {
            return false
        }
        return false
    }

    var magnitude: Int { abs(x) + abs(y) + abs(z) }
}

var detectedBeacons: [[[Vector3]]] = []
for scannerList in input.components(separatedBy: "\n\n") {
    var vectors: [Vector3] = []
    for line in scannerList.components(separatedBy: "\n").dropFirst() {
        let coords = line.components(separatedBy: ",").compactMap { Int($0) }
        if coords.isEmpty {
            continue
        }
        vectors.append(Vector3(x: coords[0], y: coords[1], z: coords[2]))
    }
    detectedBeacons.append((0..<24).map { orientation in vectors.map { $0.rotated(orientation) } } )
}

var knownBeacons = [Set(detectedBeacons[0][0])]
detectedBeacons.remove(at: 0)
var offsets = [Vector3(x: 0, y: 0, z: 0)]

var referenceIndex = 0
while !detectedBeacons.isEmpty {
    trial: for testIndex in (0..<detectedBeacons.count).reversed() {
        for (orientation, testBeacons) in detectedBeacons[testIndex].enumerated() {
            for referenceBeacon in knownBeacons[referenceIndex] {
                for testBeacon in testBeacons {
                    let offset = referenceBeacon - testBeacon
                    let translatedBeacons = Set(testBeacons.map { $0 + offset })
                    if knownBeacons[referenceIndex].intersection(translatedBeacons).count >= 12 {
                        print(testIndex, orientation, offset)
                        knownBeacons.append(translatedBeacons)
                        detectedBeacons.remove(at: testIndex)
                        offsets.append(offset)
                        continue trial
                    }
                }
            }
        }
    }

    referenceIndex += 1
    if referenceIndex >= knownBeacons.count {
        print("oops we died")
        break
    }
}

print(knownBeacons.reduce(into: Set()) { $0.formUnion($1) }.count)


// Part 2

print("--------------")

var maxDistance = Int.min
for offset1 in offsets {
    for offset2 in offsets {
        let distance = (offset1 - offset2).magnitude
        if distance > maxDistance {
            print(offset1, offset2, distance)
            maxDistance = distance
        }
    }
}
