Day 19: Beacon Scanner
======================

https://adventofcode.com/2021/day/19

Absolutely disgusting problem today. I spent a while trying to figure out some clever way to compile the relative distances between beacons so they could be used as a way to compare overlaps, but everything I came up with ended up doing a whole metric ton of comparisons of everything in every orientation anyway, so I just gave up on that and went back to brute force.

We pick the first scanner and treat its orientation as canon, and add all its beacons to a set of known beacons. Then, we find a scanner that overlaps with the known beacons in at least 12 spots and add all those beacons to the known set, and keep going until we've added every scanner.

To determine if a scanner overlaps with the known beacons, we take that scanner's beacons and generate every rotation of its points. Then we go through each rotation one at a time, take every pair of a known beacon and a beacon from that rotation, and translate the rotation so those two beacons overlap. If we meet the requisite number of overlaps, then those translated points are the locations of all those beacons in "correct space."

For part 2, we just save those translation offsets we found in part 1 and do pairwise math on them.

Since part 1 takes several minutes to execute, I hardcoded the sequence of scanners and rotations to consider to significantly speed it up after I got the answer. The commented-out loop will do the brute force.

BUGFIX: I realized that we need to include an offset of (0, 0, 0) in the array too, because theoretically the first scanner we pick could be one of the two that are the farthest apart.

UPDATE: made an optimized version that takes almost ten minutes less time to run than my first solution (though that's still a two minute runtime). The easy optimization was to precalculate all rotations ahead of time rather than doing them repeatedly. The architectural optimization was to change the overall procedure to be more like what the problem actually says to do:

We pick the first scanner and treat its orientation as canon, and add its beacons to a list of sets of standardized beacons. Then, we take the first set of standardized beacons and compare it with every rotation of every unidentified scanner to see if there are 12 overlaps, and if there are, we remove it from the consideration list and add its set rotated and translated beacons to the list of sets. Then, we move on to the second set in the list of standardized beacons, and keep going until we've found everything. The optimization here is that in the first approach we keep comparing the same beacons to each other over and over again, whereas with this approach we don't ever repeat a set of beacon comparisons.

* Part 1: 1110th place (2:32:17)
* Part 2: 1076th place (2:46:18)
