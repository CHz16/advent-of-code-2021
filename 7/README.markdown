Day 7: The Treachery of Whales
==============================

https://adventofcode.com/2021/day/7

Absolutely just brute forced this bad boy: go through every possible coordinate in between the crabs with the smallest and largest positions and calculate the fuel for each one. Doing a binary search or something would assuredly be much faster, but that would've taken effort to code and I'm also not 100% sure that the fuel calculation is bitonic

UPDATE: Felt bad about how long this one took so I came back and optimized the search. The point with the minimal overall absolute deviation is the median, and the point with the minimal overall squared error is the mean: https://www.johnmyleswhite.com/notebook/2013/03/22/modes-medians-and-means-an-unifying-perspective/. However, since the error function in part 2 isn't actually the squared error but rather `error * (error + 1) / 2`, the rounded mean doesn't give us the correct answer in all cases necessarily (including my case), so for laughs I just have it iterate through a few numbers higher and lower than the mean and hope that finds the actual minimum.

UPDATE UPDATE: I hated the "lawl just expand the range" approach so instead I made it smarter, and it'll start from the mean and go up or down until it finds a local minimum. This does assume that the cost function is concave down, which might not be safe but we'll go with it. This is the last time I'll touch this I promise

* Part 1: 306th place (2:41)
* Part 2: 125th place (3:41)
