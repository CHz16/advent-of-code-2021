//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "7", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

let crabs = input.components(separatedBy: "\n")[0].components(separatedBy: ",").compactMap { Int($0) }.sorted()

let median: Int
if crabs.count % 2 == 1 {
    median = crabs[crabs.count / 2]
} else {
    median = (crabs[crabs.count / 2] + crabs[crabs.count / 2 + 1]) / 2
}
print(crabs.reduce(0) { $0 + abs(median - $1) })


// Part 2

print("--------------")

func part2Cost(for position: Int) -> Int {
    return crabs.reduce(0) { sum, crab in
        let distance = abs(position - crab)
        return sum + distance * (distance + 1) / 2
    }
}

var position = crabs.reduce(0, +) / crabs.count
var currentFuel = part2Cost(for: position)
var direction = 1, nextFuel = part2Cost(for: position + direction)
if nextFuel > currentFuel {
    direction = -1
    nextFuel = part2Cost(for: position + direction)
    if nextFuel > currentFuel {
        direction = 0
    }
}

if direction != 0 {
    position += direction
    repeat {
        currentFuel = nextFuel
        position += direction
        nextFuel = part2Cost(for: position)
    } while nextFuel <= currentFuel
}
print(currentFuel)
