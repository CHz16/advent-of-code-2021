//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "7", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

let crabs = input.components(separatedBy: "\n")[0].components(separatedBy: ",").compactMap { Int($0) }.sorted()

let median: Int
if crabs.count % 2 == 1 {
    median = crabs[crabs.count / 2]
} else {
    median = (crabs[crabs.count / 2] + crabs[crabs.count / 2 + 1]) / 2
}
print(crabs.reduce(0) { $0 + abs(median - $1) })


// Part 2

print("--------------")

let mean = crabs.reduce(0, +) / crabs.count
print(((mean - 5)...(mean + 5)).map { position in crabs.reduce(0) { sum, crab in
        let distance = abs(position - crab)
        return sum + distance * (distance + 1) / 2
}}.min()!)
