//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "7", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

let crabs = input.components(separatedBy: "\n")[0].components(separatedBy: ",").compactMap { Int($0) }
let minCrab = crabs.min()!, maxCrab = crabs.max()!

var minFuel = Int.max
for i in (minCrab + 1)..<maxCrab {
    var fuel = 0
    for crab in crabs {
        fuel += abs(i - crab)
    }
    if fuel < minFuel {
        print(i, fuel)
        minFuel = fuel
    }
}


// Part 2

print("--------------")

minFuel = Int.max
for i in (minCrab + 1)..<maxCrab {
    var fuel = 0
    for crab in crabs {
        let distance = abs(i - crab)
        fuel += (distance * (distance + 1)) / 2
    }
    if fuel < minFuel {
        print(i, fuel)
        minFuel = fuel
    }
}
