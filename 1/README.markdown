Day 1: Sonar Sweep
==================

https://adventofcode.com/2021/day/1

Part 1 is just comparing two consecutive numbers in a list, extremely simple. Part 2 would also be very simple to implement exactly how it asks: find the sums of three consecutive numbers and compare them. But as an optimization, since the sums will share two numbers, all we have to do is compare the unique numbers in each sum and find which one is bigger. So if we're comparing the windows `i-i+2` and `i+1-i+3`, then we just have to compare `i` and `i+3`.
