//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "1", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

let samples = input.components(separatedBy: "\n").compactMap { Int($0) }

var increases = 0
for i in 1..<samples.count {
    if samples[i] - samples[i - 1] > 0 {
        increases += 1
    }
}
print(increases)


// Part 2

print("--------------")

increases = 0
for i in 3..<samples.count {
    if samples[i] - samples[i - 3] > 0 {
        increases += 1
    }
}
print(increases)
