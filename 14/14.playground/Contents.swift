//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "14", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

let lines = input.components(separatedBy: "\n")

var pairs: [String: Int] = [:]
var index = lines[0].startIndex
while index < lines[0].index(before: lines[0].endIndex) {
    let nextIndex = lines[0].index(after: index)
    pairs[String(lines[0][index...nextIndex]), default: 0] += 1
    index = nextIndex
}
let firstChar = lines[0][lines[0].startIndex], lastChar = lines[0][lines[0].index(before: lines[0].endIndex)]

var results: [String: [String]] = [:]
for line in lines.dropFirst(2) {
    if line.isEmpty {
        continue
    }
    let firstCharacter = line[line.startIndex]
    let secondCharacter = line[line.index(after: line.startIndex)]
    let thirdCharacter = line[line.index(before: line.endIndex)]
    results[String([firstCharacter, secondCharacter])] = [String([firstCharacter, thirdCharacter]), String([thirdCharacter, secondCharacter])]
}

var currentPairs = pairs
for _ in 0..<10 {
    var nextPairs: [String: Int] = [:]
    for (pair, count) in currentPairs {
        for result in results[pair]! {
            nextPairs[result, default: 0] += count
        }
    }
    currentPairs = nextPairs
}

var concordance: [Character: Int] = [:]
for (pair, count) in currentPairs {
    concordance[pair[pair.startIndex], default: 0] += count
    concordance[pair[pair.index(before: pair.endIndex)], default: 0] += count
}
for (char, count) in concordance {
    if char == firstChar || char == lastChar {
        concordance[char] = (count - 1) / 2 + 1
    } else {
        concordance[char] = count / 2
    }
}
print(concordance.values.max()! - concordance.values.min()!)


// Part 2

print("--------------")

for _ in 0..<30 {
    var nextPairs: [String: Int] = [:]
    for (pair, count) in currentPairs {
        for result in results[pair]! {
            nextPairs[result, default: 0] += count
        }
    }
    currentPairs = nextPairs
}

concordance = [:]
for (pair, count) in currentPairs {
    concordance[pair[pair.startIndex], default: 0] += count
    concordance[pair[pair.index(before: pair.endIndex)], default: 0] += count
}
for (char, count) in concordance {
    if char == firstChar || char == lastChar {
        concordance[char] = (count - 1) / 2 + 1
    } else {
        concordance[char] = count / 2
    }
}
print(concordance.values.max()! - concordance.values.min()!)
