Day 14: Extended Polymerization
===============================

https://adventofcode.com/2021/day/14

I guessed from the problem that this would be "part 2 will be doing it enough times that brute forcing will never stop executing," so I decided to figure out a Smart Way to do it from the beginning. As a result, part 2 took absolutely no time to add on and I jumped up big in the rankings (though still well off the leaderboard, as usual).

The trick to this puzzle is that you don't have to keep track of the full string; instead, you just need to keep track of how many of each pair of characters there are. Then, the step to generate the next "string" is to replace each pair with the two pairs it creates according to its insertion rule. For example:

```
NCNBCHB

NC -> B
CN -> C
NB -> B
BC -> B
CH -> B
HB -> C
```

We first turn the starting string into a pair list: `BC` 1, `CH` 1, `CN` 1, `HB` 1, `NB` 1, `NC` 1.

The first rule `NC -> B` says to turn `NC` into `NBC`, or in other words, replace `NC` with `NB` and `BC`. Applying each of those rules to the pair list, we get the following pair list: `BB` 2, `BC` 2, `BH` 1, `CB` 2, `CC` 1, `CN` 1, `HC` 1, `NB` 2. If you take the actual string result of `NBCCNBBBCBHCB` and calculate its pair list, you'll find they match.

This turns the processing step into just doing some math on a dictionary, which is fast and easy. The complication is that, to count how many of each character occurs like the problem wants, we can't just sum the characters of every pair, because pairs in the string share characters and we'll double count. In fact, we'll double count **every** single character except the first and last one. Luckily, because we always insert characters in the middle, the first and last characters will never change, so we can just save what those are at the beginning and factor them into our math.

* Part 1: 3009th place (18:47)
* Part 2: 321st place (19:27)
